$(document).ready(function () {
  let acc = document.getElementsByClassName("accordion");
  let panels = document.querySelectorAll('.panel');
  for (let i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function () {
      for(let i = 0; i < acc.length; i++) {
        acc[i].classList.remove('active');
        // if(acc[i].children[1]) {
        //   acc[i].children[1].style.transform = 'rotate(0deg)';
        // }
      }
      let accArrowRotate = this.children[1];
      if(accArrowRotate) {
        // if(accArrowRotate.classList.contains('accordion-arrow')) {
        //   accArrowRotate.style.transform = 'rotate(180deg)';
        // }
      }
      this.classList.add("active");
      let panel = this.nextElementSibling;
      if (panel.style.maxHeight) {
          panel.style.maxHeight = null;
          if(accArrowRotate) {
            // if(accArrowRotate.classList.contains('accordion-arrow')) {
            //   accArrowRotate.style.transform = 'rotate(0deg)';
            // }
          }
      } else {
        // First close all accordion panels
        for(let i = 0; i < panels.length; i++) {
          panels[i].style.maxHeight = null;
        }
        // And then just open the one that is clicked
        panel.style.maxHeight = panel.scrollHeight + "px";
        if(accArrowRotate) {
          // if(accArrowRotate.classList.contains('accordion-arrow')) {
          //   accArrowRotate.style.transform = 'rotate(180deg)';
          // }
        }
      }
    });
  }


});
// END: Accordion for FAQ

