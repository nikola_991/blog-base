function openCropperModal() {
    document.getElementById("cropperModal").classList.add("opened");
}

function closeCropperModal() {
    document.getElementById("cropperModal").classList.remove("opened");
}

$(document).ready(function () {
    let article = document.querySelectorAll('.page-wrapper');
    for (let j = 0; j < article.length; j++) {

        let postID = article[j].getAttribute('id');
        let inputEl = document.createElement('input');
        //
        inputEl.setAttribute("type", "file");
        // inputEl.setAttribute("id", `input-${postID}`);
        article[j].appendChild(inputEl);
        inputEl.style.display = 'none';

        let imgs = article[j].querySelectorAll('.image');

        imgs.forEach(i => {
            i.addEventListener('click', function () {
                if (localStorage.getItem('edit') === 'true') {
                    inputEl.click()
                }
            });

            let canvas = $("#canvas");
            let context = canvas.get(0).getContext("2d");
            let result = $('#result');

            $(inputEl).on('change', function () {

                if (this.files && this.files[0]) {
                    if (this.files[0].type.match(/^image\//)) {
                        let reader = new FileReader();
                        reader.onload = function (evt) {
                            let img = new Image();
                            img.onload = function () {
                                context.canvas.height = img.height;
                                context.canvas.width = img.width;
                                context.drawImage(img, 0, 0);
                                let cropper = canvas.cropper({
                                    aspectRatio: $(inputEl).parent().find('.image').width() / $(inputEl).parent().find('.image').height(),
                                    viewMode: 1,
                                    fillColor: '#fff',
                                    crop(e) {
                                        document.getElementById('widthImage').textContent = e.detail.width.toFixed(0);
                                        document.getElementById('heightImage').textContent = e.detail.height.toFixed(0);
                                    }
                                });
                                setTimeout(() => {
                                    openCropperModal()
                                }, 200);
                                $('#btnRestore').click(function () {
                                    canvas.cropper('destroy');
                                    result.empty();
                                    closeCropperModal()
                                });
                                $('#btnCloseCropper').click(function () {
                                    canvas.cropper('destroy');
                                    result.empty();
                                    closeCropperModal()
                                });
                            };
                            img.src = evt.target.result;
                        };
                        reader.readAsDataURL(this.files[0]);


                        const cropImageBtn = document.querySelector('#crop-image');
                        cropImageBtn.addEventListener('click', function (art) {
                            let croppedImageDataURL = canvas.cropper('getCroppedCanvas', {fillColor: '#FFFFFF'}).toDataURL("image/jpeg", 0.9);
                            result.append($('<img>').attr('src', croppedImageDataURL));
                            // console.log(croppedImageDataURL);
                            let original_name = inputEl.files[0].name;

                            const _data = {
                                id_post: postID,
                                image: croppedImageDataURL,
                                original_name: original_name,
                            };


                            canvas.cropper('destroy');
                            result.empty();
                            i.src = croppedImageDataURL;
                            // removeSpinnerLoader();
                            $(inputEl).val('');
                            closeCropperModal();


                            document.querySelector('#save-all').addEventListener('click', function () {
                                let url = '';
                                if (article[j].getAttribute('data-page') === 'page-image') {
                                    url = `/api/upload-blog-temp-image/${postID}`
                                } else {
                                    url = `/api/upload-blog-image/${postID}`
                                }
                                $.ajax({
                                    url: url,
                                    method: 'POST',
                                    data: _data,
                                    headers: {"Authorization": localStorage.getItem('token')}
                                }).done(function (res) {
                                    console.log(res);
                                }).fail(function (err) {
                                    console.log('Error: ', err);
                                })
                            })

                        })
                    } else {
                        console.log("Invalid file type! Please select an image file.");
                    }
                } else {
                    console.log('No file(s) selected.');
                }
            })
        })
    }
});