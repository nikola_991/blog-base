const shareModal = document.getElementById('shareModal');
if (shareModal) {
    const openShareModal = document.getElementById('openShareModal');
    const closeShareModal = document.getElementById('closeShareModal');
    openShareModal.addEventListener('click', function () {
        shareModal.classList.add("opened");
        document.getElementById('shareUrl').innerHTML = window.location.href;
        document.getElementById('shareUrl').setAttribute('href', window.location.href)
    });

    closeShareModal.addEventListener('click', function () {
        shareModal.classList.remove("opened");
    });
}

const addToEvent = document.getElementById('addToEvent');
if (addToEvent) {
    const openAddToEventModal = document.getElementById('openAddToEventModal');
    const closeAddToEventModal = document.getElementById('closeAddToEventModal');
    openAddToEventModal.addEventListener('click', function () {
        addToEvent.classList.add("opened");
    });
    closeAddToEventModal.addEventListener('click', function () {
        addToEvent.classList.remove("opened");
    });
}

function openAddToEvent() {
    addToEvent.classList.toggle("opened");
}

const requestBookingModal = document.getElementById('requestBookingModal');
function openRequestToBookModal() {
    requestBookingModal.classList.toggle("opened");
}
function closeRequestToBookModal() {
    requestBookingModal.classList.remove("opened");
}

const mapModal = document.getElementById('mapModal');
const mapBtn = document.getElementById('mapBtn');

function openMapModal() {
    mapModal.classList.toggle("opened");
    if (mapModal.classList.contains("opened")) {
        mapBtn.innerHTML = 'List';
        mapModal.style.zIndex = '1002';
        mapModal.style.display = 'block';
    } else {
        mapBtn.innerHTML = 'Map'
        mapModal.style.display = 'none';
    }
}

const getDirection = document.getElementById('get-direction')
const closeGetDirection = document.getElementById('close-get-direction')
const directionModal = document.getElementById('direction-modal')

if (getDirection) {
    getDirection.addEventListener('click', function () {
        directionModal.classList.toggle("opened");
    });

    closeGetDirection.addEventListener('click', function () {
        directionModal.classList.remove("opened");
    });
}

let termsModal = document.getElementById("termsModal");
function openTermsModal() {
    termsModal.classList.add("opened");
}
function closeTermsModal() {
    termsModal.classList.remove("opened");
}

let addPostModal = document.getElementById("addPostModal");
function openAddPostModal() {
    addPostModal.classList.add("opened");
}
function closeAddPostsModal() {
    addPostModal.classList.remove("opened");
}


let cropperModal = document.getElementById("cropperModal");
function openCropperModal() {
    cropperModal.classList.add("opened");
}
function closeCropperModal() {
    cropperModal.classList.remove("opened");
}
