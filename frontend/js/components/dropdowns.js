$('.user__dropdown').click(function (e) {
    var currentDropdown = $(this).children('.user__dropdown-content');
    $('.user__dropdown-content').not(currentDropdown).fadeOut(200);
    currentDropdown.fadeToggle(200);
    $('.filter__dropdown-content').fadeOut(200);
    e.stopPropagation();
});

$('.filter__dropdown').click(function (e) {
    var currentDropdown = $(this).children('.filter__dropdown-content');
    $('.filter__dropdown-content').not(currentDropdown).fadeOut(200);
    currentDropdown.fadeToggle(200);
    $('.user__dropdown-content').fadeOut(200);
    e.stopPropagation();
});

$(window).click(function (e) {
    $('.filter__dropdown-content').fadeOut(200);
    $('.user__dropdown-content').fadeOut(200);
    e.stopPropagation();
});

