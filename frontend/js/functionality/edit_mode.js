let editable = false;

if (document.getElementById('open-edit')) {
    document.getElementById('open-edit').addEventListener('click', enterEditMode);

}
if (document.getElementById('close-edit')) {
    document.getElementById('close-edit').addEventListener('click', exitEditMode);
}

function toggleBlue() {
    $('.page-wrapper').click(function (e) {
        var currentPost = $(this).find('h4');
        $('h4').not(currentPost).removeClass('pen');
        $('h4').siblings('p').not(currentPost).css('background-color', 'transparent');
        $('h4').not(currentPost).css('background-color', 'transparent');

        if (editable) {
            currentPost.addClass('pen');
            currentPost.css('background-color', 'rgba(136, 212, 255, 0.2)');
            currentPost.siblings('p').css('background-color', 'rgba(136, 212, 255, 0.2)');
        }
        e.stopPropagation();
    });
}

function enterEditMode() {
    toggleBlue();
    editable = true;
    localStorage.setItem('edit', 'true');
    $('#open-edit').css('display', 'none');
    $('#close-edit').css('display', 'flex');
    $('#save-all').css('display', 'flex');
    $('.editable-content').attr("contenteditable", true).css('outline', '1.5px dashed #3EB7FC').css('outline-offset', '3px');
    $('#toolbarLocation').css('display', 'block');
    $('.image').css('cursor', 'pointer');
    var elements = CKEDITOR.document.find('.editable-content'),
        i = 0,
        element;

    for (let name in CKEDITOR.instances) {
        delete CKEDITOR.instances[name];
    }

    // CKEDITOR.disableAutoInline = true;
    while ((element = elements.getItem(i++))) {
        CKEDITOR.inline(element, {
            sharedSpaces: {
                top: 'toolbarLocation'
            }
        });
    }

    // document.querySelectorAll('.cover-image').forEach(img => {
    //     img.setAttribute('src', '/static/img/pb.png')
    // })

}

function exitEditMode() {
    editable = false;
    localStorage.setItem('edit', 'false');

    $('#open-edit').css('display', 'flex');
    $('#close-edit').css('display', 'none');
    $('#save-all').css('display', 'none');
    $('.editable-content').attr("contenteditable", false).css('background', 'transparent').css('outline', 'none').css('outline-offset', '0');
    $('#toolbarLocation').css('display', 'none');
    $('.image').css('cursor', 'default');
    for (let name in CKEDITOR.instances) {
        delete CKEDITOR.instances[name];
    }
}


window.addEventListener("load", function () {
    if (localStorage.getItem('edit') === 'true' && localStorage.getItem('token')) {
        enterEditMode();
        toggleBlue()
    } else {
        exitEditMode();
    }
});

if (localStorage.getItem('token') && localStorage.getItem('edit') === 'true') {
    enterEditMode();
    toggleBlue()
} else {
    exitEditMode()
}
//
// document.querySelectorAll('.post-title').forEach(title => {
//     if (title.textContent.length === 0) {
//         title.style.display = 'none';
//     } else {
//         title.style.marginBottom = '-15px'
//     }
// });

