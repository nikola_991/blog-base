// copy link from url
function copyLink() {
    let link = document.getElementById('shareUrl');
    link.value = window.location.href.toString();
    link.select();
    document.execCommand("copy");
    toaster('Kopiran URL', 'success');
}


function PopupCenter(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    let dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screenX;
    let dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screenY;

    let width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    let height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    let systemZoom = width / window.screen.availWidth;
    let left = (width - w) / 2 / systemZoom + dualScreenLeft;
    let top = (height - h) / 2 / systemZoom + dualScreenTop;
    let newWindow = window.open(url, title, 'scrollbars=yes, width=' + w / systemZoom + ', height=' + h / systemZoom + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) newWindow.focus();
}

function openSocialWindow(url) {
    PopupCenter(url, 'Share via', '600', '600');
}

let currentPageUrl = encodeURIComponent(document.URL);
// let tweet = encodeURIComponent($("meta[property='og:title']").attr("content"));

$(".facebook").on("click", function (url) {
    url = "https://www.facebook.com/sharer.php?u=" + currentPageUrl;
    openSocialWindow(url);
});

$(".twitter").on("click", function (url) {
    url = "https://twitter.com/intent/tweet?url=" + currentPageUrl;
    openSocialWindow(url);
});


$(".linkedin").on("click", function () {
    url = "http://www.linkedin.com/shareArticle?mini=true&url=" + currentPageUrl;
    openSocialWindow(url);
});