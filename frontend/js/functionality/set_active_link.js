(function setActiveLink() {
    let path = window.location.pathname;
    path = path.replace(/\/$/, "");
    path = decodeURIComponent(path);

    $("nav li a").each(function () {

        let href = null;
        try {
            href = $(this).attr('href');
        } catch (err) {
            console.log('err');
        }
        if (href) {
            if (path.substring(0, href.length) === href) {
                $(this).closest('nav li a').addClass('active-nav');
            }
        }
    });
})();