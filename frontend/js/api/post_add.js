function addPost() {
    let postTitle = document.getElementById('post-title');
    let postSubtitle = document.getElementById('post-subtitle');
    let postIntro = document.getElementById('post-intro');
    let postCategory = document.getElementById('post-category');
    // let postSource = document.getElementById('post-source');
    const _data = {
        title: postTitle.value,
        subtitle: postSubtitle.value,
        category: postCategory.value,
        intro: postIntro.value,
        // source: postSource.value,
        body: 'enter the post body',
    };
    if (!postTitle.value || postTitle.value === '') {
        // toaster('Title field is empty', 'warning');
    } else {
        $.ajax({
            url: '/api/wiki/posts',
            method: 'PUT',
            data: _data,
            // dataType: 'json'
        }).done(function (res) {
            console.log(res);
            // toaster('post is created', 'success');
            closeAddPostsModal();
            checkCategory()
            let slug = postTitle.value.toLowerCase().trim().replace(/[^a-z0-9_]+/g, "-")
            setTimeout(() => {
                window.location.href = '/news/' + slug
            }, 1000)
        }).fail(function (err) {
            console.log('Error: ', err);
        })
    }
}

function checkCategory() {
    let category = document.getElementById('post-category')
    console.log(category.value)
}
