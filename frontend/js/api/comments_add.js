function addComment(id_post, author) {
    let commentText = document.getElementById('comment-text');


    const _data = {
        id_post: id_post,
        text: commentText.value,
        display_name: author
    };

    if (!commentText.value || commentText.value === '') {
        // toaster('Comment field is empty', 'warning');
        console.log('ne valja')

    } else {
        $.ajax({
            url: `/api/wiki/posts/${id_post}/comments`,
            method: 'PUT',
            data: _data,
            dataType: 'json'
        }).done(function (res) {
            console.log(res);
            // toaster('comment is sent', 'success');
        }).fail(function (err) {
            console.log('Error: ', err);

        })
    }
}

