if (document.getElementById('add-post')) {


    document.getElementById('add-post').addEventListener('click', function (e) {
        e.preventDefault();
        let title = document.getElementById('post-title').value;
        let body = 'Odit est odis etur seces illis sim dolupta nobit unditate corepernam rem suntem quaepta simolup tatium nulluptatium dit ped magnatur re moluptat. Tus que occus, et omniam fugitatatur sunt aut ex excest, vendam quatiat. Itasi officie ntemodio. Udandel luptae il incimpo rehentiume milique ipsae a cum et pori nullis aut ipsumque simaiosandes volecea quassinum sitatiandis est dolupta tionsequo essunt. Odit est odis etur seces illis sim dolupta nobit unditate corepernam rem suntem quaepta simolup tatium nulluptatium dit ped magnatur re moluptat. Tus que occus, et omniam fugitatatur sunt aut ex excest, vendam quatiat. Itasi officie ntemodio.';
        let template_lang = document.getElementById('select-language').value;
        // let idTemplate = document.querySelector('input[name="radio-thumb"]:checked') ?
        //     document.querySelector('input[name="radio-thumb"]:checked').value : 'template1';

        const _data = {
            id_template: 'template1',
            title: title,
            body: body,
            slug: string_to_slug(title),
            language: template_lang
        };

        $.ajax({
            url: `/api/site/template/template1/pages`,
            method: 'PUT',
            data: _data,
            dataType: 'json'
        }).done(function (res) {
            console.log(res);
            // window.location.href = `/${template_lang}/products/${slug}`
        }).fail(function (err) {
            console.log('Error: ', err);
            return err
        })
    })
}

function string_to_slug(str) {
    str = str.replace(/^\s+|\s+$/g, "");
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    let from = "åàáãäâèéëêìíïîòóöôùúüûñçćčđšž·/_,:;";
    let to = "aaaaaaeeeeiiiioooouuuuncccdsz------";

    for (let i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
    }

    str = str
        .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
        .replace(/\s+/g, "-") // collapse whitespace and replace by -
        .replace(/-+/g, "-") // collapse dashes
        .replace(/^-+/, "") // trim - from start of text
        .replace(/-+$/, ""); // trim - from end of text

    return str;
}


(function () {

    var doc = document.documentElement;
    var w = window;

    var prevScroll = w.scrollY || doc.scrollTop;
    var curScroll;
    var direction = 0;
    var prevDirection = 0;

    var header = document.getElementById('site-header');

    var checkScroll = function () {

        /*
        ** Find the direction of scroll
        ** 0 - initial, 1 - up, 2 - down
        */

        curScroll = w.scrollY || doc.scrollTop;
        if (curScroll > prevScroll) {
            //scrolled up
            direction = 2;
        } else if (curScroll < prevScroll) {
            //scrolled down
            direction = 1;
        }

        if (direction !== prevDirection) {
            toggleHeader(direction, curScroll);
        }

        prevScroll = curScroll;
    };

    var toggleHeader = function (direction, curScroll) {
        if (direction === 2 && curScroll > 52) {

            //replace 52 with the height of your header in px

            header.classList.add('hide');
            prevDirection = direction;
        } else if (direction === 1) {
            header.classList.remove('hide');
            prevDirection = direction;
        }
    };

    window.addEventListener('scroll', checkScroll);

})();
const saveAll = document.querySelector('#save-all');

if (saveAll) {
    document.querySelectorAll('.post-wrapper').forEach(art => {
        saveAll.addEventListener('click', el => {
            let postID = art.getAttribute('id');
            let element = document.getElementById(postID);
            let title = element.getElementsByTagName('h4')[0].textContent;
            let body = element.getElementsByTagName('p')[0].innerHTML;

            const _data = {
                id: postID,
                title: title,
                body: body,
                language: 'sr',
            };

            console.log(_data);

            const url = `/api/wiki/posts/${postID}`;
            let fetchData = {
                method: 'PATCH',
                body: JSON.stringify(_data),
                headers: new Headers({
                    'Authorization': localStorage.getItem('token'),
                    'Content-Type': 'application/json'
                })
            };
            fetch(url, fetchData)
                .then(res => {
                    console.log(res);
                })
                .catch(err => {
                    console.log(err)
                });
        });
    })

    document.getElementById('save-all').addEventListener('click', function () {
        let pageId = document.querySelector('.news-article').getAttribute('data-page-id')

        const _data_page = {
            id: pageId,
            title: document.querySelector('.page-title').textContent,
            body: document.querySelector('.page-body').innerHTML,
            lang: 'sr',
        };

        const url_page = `/api/site/template/${pageId}/pages`;
        let fetchDataPage = {
            method: 'PATCH',
            body: JSON.stringify(_data_page),
            headers: new Headers({
                'Authorization': localStorage.getItem('token'),
                'Content-Type': 'application/json'
            })
        };
        fetch(url_page, fetchDataPage)
            .then(res => {
                console.log(res)
            })
            .catch(err => {
                console.log(err)
            });
    })
}




