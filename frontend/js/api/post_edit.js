function editPost(postId) {
    const _data = {
        id: postId,
        title: document.getElementById('edit-title').textContent,
        subtitle: document.getElementById('edit-subtitle').textContent,
        body: document.getElementById('edit-body').innerHTML,
        // category: document.getElementById('edit-category').textContent,
        // source: document.getElementById('edit-source').textContent,
    };
    $.ajax({
        url: `/api/wiki/posts/${postId}`,
        method: 'PATCH',
        data: _data,
        dataType: 'json'
    }).done(function (res) {
        console.log('updated')
        // toaster('post is successfully edited', 'success');
    }).fail(function (err) {
        console.log('Error: ', err);
    })
}