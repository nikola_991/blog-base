
function publishPost(postId) {
    let btn = document.getElementById('post-publish');
    const _data = {
        id: postId,
        status: 3
    };

    console.log(_data)
    $.ajax({
        url: `/api/wiki/posts/${postId}`,
        method: 'PATCH',
        data: _data,
        dataType: 'json'
    }).done(function (res) {
        // toaster('post is published', 'success');
        setTimeout(() => {
            // window.location.href = '/news/';
        }, 1000);
    }).fail(function (err) {
        console.log('Error: ', err);
    })
}