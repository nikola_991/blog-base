let token = localStorage.getItem('token');

let checkSlugBtn = document.getElementById('check-slug');
let addBtn = document.getElementById('add-post');
let newSlug = document.getElementById('new-slug');
let badSlug = document.getElementById('bad-slug');
let postTitle = document.getElementById('post-title');

if (addBtn && checkSlugBtn) {
    addBtn.style.backgroundColor = '#dfdfdf';
    addBtn.disabled = true;
    addBtn.style.cursor = 'not-allowed';
    checkSlugBtn.style.backgroundColor = '#dfdfdf';
    checkSlugBtn.disabled = true;
    checkSlugBtn.style.cursor = 'not-allowed';
}

let checkedTemplate = '';
document.querySelectorAll('.image-radio input').forEach(e => {
    e.addEventListener('click', function () {
        checkedTemplate = e.value;
    })
});

export function checkInput() {
    let title = document.getElementById('post-title').value;
    let slug = document.getElementById('new-slug');
    slug.disabled = true;
    slug.value = string_to_slug(title);

    if (checkedTemplate === '' || slug.value === '') {
        addBtn.style.backgroundColor = '#dfdfdf';
        addBtn.disabled = true;
        addBtn.style.cursor = 'not-allowed';
        checkSlugBtn.style.backgroundColor = '#dfdfdf';
        checkSlugBtn.disabled = true;
        checkSlugBtn.style.cursor = 'not-allowed';
        newSlug.style.color = 'rgba(0, 0, 0, 0.6)';
    } else {
        checkSlugBtn.style.backgroundColor = '#f48925';
        checkSlugBtn.disabled = false;
        checkSlugBtn.style.cursor = 'pointer';
    }
    // console.log(title)
}

if (postTitle) {
    postTitle.addEventListener('keyup', function () {
        if (badSlug.style.visibility === 'visible') {
            addBtn.disabled = true;
        }
        checkInput();
    });
    window.addEventListener('change', function () {
        if (badSlug.style.visibility === 'visible' || checkedTemplate !== '') {
            addBtn.disabled = true;
        }
        checkInput()
    });
}

if (checkSlugBtn) {
    checkSlugBtn.addEventListener('click', function () {
        let title = document.getElementById('post-title').value;
        let slug = newSlug.value = string_to_slug(title)

        $.ajax({
            url: "/api/wiki/check-slug",
            headers: {
                "Authorization": token
            },
            data: {
                slug: slug
            },
            method: "GET",
            success: function (r) {
                console.log('CHECK SLUG', JSON.parse(r).slug);
                newSlug.style.color = 'green';
                addBtn.style.backgroundColor = '#f48925';
                addBtn.disabled = false;
                addBtn.style.cursor = 'pointer';
                badSlug.style.visibility = 'hidden';
            },
            error: function (err) {
                console.log("CHECK SLUG ERROR", err);
                newSlug.style.color = 'red';
                badSlug.textContent = 'slug already exist, please change and check again';
                badSlug.style.visibility = 'visible';
                addBtn.style.backgroundColor = '#dfdfdf';
                addBtn.disabled = true;
                addBtn.style.cursor = 'not-allowed';
                setTimeout(() => {
                    checkSlugBtn.style.backgroundColor = '#dfdfdf';
                    checkSlugBtn.disabled = true;
                    checkSlugBtn.style.cursor = 'not-allowed';
                    checkedTemplate = '';
                    $(".image-radio-checked").removeClass("image-radio-checked");
                    postTitle.value = '';
                    newSlug.value = '';
                    newSlug.style.color = 'rgba(0, 0, 0, 0.6)';
                    badSlug.style.visibility = 'hidden';
                }, 3000)
            }
        });
    });
}

export function string_to_slug(str) {
    str = str.replace(/^\s+|\s+$/g, "");
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    let from = "åàáãäâèéëêìíïîòóöôùúüûñçćčđšž·/_,:;";
    let to = "aaaaaaeeeeiiiioooouuuuncccdsz------";

    for (let i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
    }

    str = str
        .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
        .replace(/\s+/g, "-") // collapse whitespace and replace by -
        .replace(/-+/g, "-") // collapse dashes
        .replace(/^-+/, "") // trim - from start of text
        .replace(/-+$/, ""); // trim - from end of text

    return str;
}

