function deletePost(postId) {
    const _data = {
        id: postId,
    };
    $.ajax({
        url: `/api/wiki/posts`,
        method: 'DELETE',
        data: _data,
        dataType: 'json'
    }).done(function (res) {
        // toaster('post is successfully deleted', 'success');
        setTimeout(() => {
            window.location.href = '/news'
        }, 1000)
    }).fail(function (err) {
        console.log('Error: ', err);
    })
}



