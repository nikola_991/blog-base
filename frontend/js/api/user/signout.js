
function logout() {
    const token = localStorage.getItem('token');
    const url = '/user/logout';
    let fetchData = {
        method: 'POST',
        headers: new Headers({'Authorization': token})
    };
    fetch(url, fetchData)
        .then(res => {
            console.log(res);
            $("#admin-header").css('display', 'none');
            // closeEditMode();
            localStorage.removeItem('token');
            localStorage.removeItem('edit');
            window.location.href = '/'
        })
        .catch(err => {
            console.log(err)
        })
}

function autoLogout() {
    let t;
    window.onload = resetTimer;
    window.onmousemove = resetTimer;
    window.onmousedown = resetTimer;
    window.ontouchstart = resetTimer;
    window.onclick = resetTimer;
    window.onkeypress = resetTimer;
    window.addEventListener('scroll', resetTimer, true);

    function resetTimer() {
        clearTimeout(t);
        t = setTimeout(logout, 600000);  // 10 minutes
    }
}

autoLogout();



