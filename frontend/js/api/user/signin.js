let emailRegex = new RegExp(/^[^\s@]+@[^\s@]+\.[^\s@]+$/);
let passRegex = new RegExp(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/)


function login(e) {
    e.preventDefault();
    const username = document.getElementById('username');
    const password = document.getElementById('password');
    let responseText = document.getElementById('response_text');

    // if (!emailRegex.test(username.value)) {
    //     responseText.textContent = 'wrong email'
    //     responseText.style.visibility = 'visible'
    //     username.value = ''
    //     password.value = ''
    //     return
    // }
    // if (!passRegex.test(password.value)) {
    //     responseText.textContent = 'wrong password'
    //     responseText.style.visibility = 'visible'
    //     username.value = ''
    //     password.value = ''
    //     return
    // }
    //
    if (!passRegex.test(password.value) && !emailRegex.test(username.value)) {
        responseText.textContent = 'email and password not valid';
        responseText.style.visibility = 'visible';
        username.value = ''
        password.value = ''
    }


    const _data = {
        username: username.value,
        password: password.value
    };


    $.ajax({
        url: `/user/login`,
        method: 'POST',
        data: JSON.stringify(_data),
        dataType: 'json'
    }).done(function (res) {
        localStorage.setItem('token', res.token);
        console.log('logovan');
        window.location.href = '/'
    }).fail(function (err) {
        console.log('Error: ', err);
        username.value = ''
        password.value = ''
    })
}


function register(e) {
    e.preventDefault();
    const username = document.getElementById('reg-username');
    const password = document.getElementById('reg-password');
    const firstName = document.getElementById('first-name');
    const lastName = document.getElementById('last-name');

    let responseText = document.getElementById('response_text');
    if (!passRegex.test(password.value) && !emailRegex.test(username.value)) {
        responseText.textContent = 'email and password not valid'
        responseText.style.visibility = 'visible'
        firstName.value = ''
        lastName.value = ''
        username.value = ''
        password.value = ''
    }

    const _data = {
        username: username.value,
        password: password.value,
        data: {
            'first_name': firstName.value,
            'last_name': lastName.value,
        }
    };

    console.log(_data)

    $.ajax({
        url: `/user/register`,
        method: 'POST',
        data: JSON.stringify(_data),
        dataType: 'json'
    }).done(function (res) {
        localStorage.setItem('token', res.token);
        console.log('logovan');
        responseText.classList.remove('error');
        responseText.textContent = '';
        // window.location.href = '/'
    }).fail(function (err) {
        console.log('Error: ', err);
        firstName.value = ''
        lastName.value = ''
        username.value = ''
        password.value = ''
    })
}


const loginBtn = document.getElementById('login');

if (loginBtn) {
    loginBtn.addEventListener('click', login)
}

const registerBtn = document.getElementById('register');

if (registerBtn) {
    registerBtn.addEventListener('click', register)
}

