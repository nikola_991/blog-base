// START map scroll in event sections
const map = document.getElementById('map');
if (map) {
    function checkOffset() {
        if ($('#map').offset().top + $('#map').height() >= $('#footer').offset().top)
            $('#map').css('position', 'sticky').css('position', '-webkit-sticky').css('overflow', 'hidden').css('width', '100%');
        if ($(document).scrollTop() + window.innerHeight < $('#footer').offset().top)
            $('#map').css('position', 'fixed').css('overflow', 'visible').css('width', '35%');
    }

    $(document).scroll(function () {
        checkOffset();
    });
}

// END map scroll in event sections

function doNothing() {
    return null
}

const mq = window.matchMedia("(min-width: 1025px)");
if (mq.matches) {
    const courseList = document.getElementById('course-list');
    if (courseList) {

        function scrollCourse() {
            if ($('#course-list').offset().top + $('#course-list').height() >= $('#footer').offset().top - 40)
                $('#course-list').css('position', 'sticky').css('top', '100px').css('width', '98%').css('right', '60px');
            if ($(document).scrollTop() + window.innerHeight < $('#footer').offset().top)
                $('#course-list').css('position', 'fixed').css('top', '0').css('width', '30%').css('right', '60px');
        }


        $(document).scroll(function () {
            scrollCourse();
        });
    }
} else {
    $(document).scroll(function () {
        doNothing();
    });
}

// const bookingFixed = document.getElementById('bookingFixed');
// // if (bookingFixed) {
// //     function booking() {
// //         if ($('#bookingFixed').offset().top + $('#bookingFixed').height() >= $('#footer').offset().top)
// //             $('#bookingFixed').css('position', 'sticky').css('z-index', '990');
// //         if ($(document).scrollTop() + window.innerHeight < $('#footer').offset().top - 50)
// //             $('#bookingFixed').css('position', 'fixed').css('z-index', '1000');
// //     }
// //
// //     $(document).scroll(function () {
// //         booking();
// //     });
// // }