// START: all cards same height
let heightSmallPrice = 0;
$('#small-card').each(function () {
    let height = $(this).height();
    if (height > heightSmallPrice) {
        heightSmallPrice = height;
    }
});
$('#small-card').height(heightSmallPrice);

// START button for show more text in card price large
const btnCardShowMore = document.querySelectorAll(".show-more-btn");
let hiddenText = document.querySelectorAll(".hiddenText");
let showText = document.querySelectorAll(".showText");
if (btnCardShowMore) {
    for (let i = 0; i < btnCardShowMore.length; i++) {
        btnCardShowMore[i].addEventListener('click', function () {
            // const dataPrice = btnCardShowMore[i].getAttribute('data-cardName');
            // console.log(dataPrice);
            if (hiddenText[i].style.display === "none") {
                hiddenText[i].style.display = "inline";
                showText[i].style.display = "none";
                btnCardShowMore[i].innerHTML = `<img src=\"/static/img/konferencije/icons/plus.png\" alt=\"\"> <span>${trans_showmore}</span>`;
            } else {
                hiddenText[i].style.display = "none";
                showText[i].style.display = "inline";
                btnCardShowMore[i].innerHTML = `<img src=\"/static/img/konferencije/icons/plus.png\" alt=\"\"> <span>${trans_showless}</span>`;
            }

        });
    }
}
// END button for show more events in card price large


// START button for show more text in similar events
const btnCardShowMoreEvents = document.getElementById("show-more-events");
if (btnCardShowMoreEvents) {
    btnCardShowMoreEvents.addEventListener('click', function () {
        let hiddenEvents = document.getElementById("hiddenEvents");
        let showEvents = document.getElementById("showEvents");
        if (hiddenEvents.style.display === "none") {
            window.location.href = '/events'
        } else {
            hiddenEvents.style.display = "none";
            showEvents.style.display = "inline";
            btnCardShowMoreEvents.innerHTML = `<img src=\"/static/img/konferencije/icons/plus.png\" alt=\"\"> <span>${trans_allEvents}</span>`;
        }
    });
}
// END button for show more events in similar events


// START button for show more text in similar events
const btnCardShowMoreHotels = document.getElementById("show-more-hotels");
if (btnCardShowMoreHotels) {
    btnCardShowMoreHotels.addEventListener('click', function () {
        let hiddenHotels = document.getElementById("hiddenHotels");
        let showHotels = document.getElementById("showHotels");
        if (hiddenHotels.style.display === "none") {
            window.location.href = '/event_spaces'
        } else {
            hiddenHotels.style.display = "none";
            showHotels.style.display = "inline";
            btnCardShowMoreHotels.innerHTML = `<img src=\"/static/img/konferencije/icons/plus.png\" alt=\"\"> <span>${trans_allEvents}</span>`;
        }
    });
}
// END button for show more events in similar events
