import os
import sys
import csv


def translate(lang, term):
    if lang == 'source':
        return term

    if not hasattr('translate', 'cache'):
        try:
            fname = os.path.dirname(os.path.realpath(__file__)) + '/' + 'translations.csv'
        except:
            print('-' * 100)
            print("error opening {} file".format(fname))
            sys.exit()

        with open(fname) as f:
            cr = csv.reader(f, delimiter=',', quotechar='"')
            ln = 0
            translate.cache = {}

            for row in cr:
                ln += 1
                if ln == 1:
                    continue  # skip header

                if row[0] in translate.cache:
                    print('-' * 100)
                    print("Invalid translate file, term {} is duplicated in line {}".format(row[0], ln))
                    sys.exit()

                translate.cache[row[0]] = {'de': row[2],
                                           'it': row[3],
                                           'en': row[4]}
                try:
                    pass
                except:
                    print('-' * 100)
                    print("Invalid translation file, in line {}, row: {}".format(ln, row))
                    sys.exit()

    if term not in translate.cache:
        return term

    if lang not in translate.cache[term]:
        return term

    return translate.cache[term][lang]
