from base.application.components import Base
from base.application.components import api
from base.application.components import params
from base.application.components import authenticated, db, readonly
# import src.models.blog as blog
from src.models.blog import Post
from sqlalchemy import desc, asc
from src.models.site import SitePage
from src.models.user import User
from tornado.template import Template, Loader
import os
from web_blog.translations import translate

languages = [
    {'id': 'en', 'text': 'english'},
    {'id': 'sr', 'text': 'serbian'}
]



@api(
    URI=['/?', '/:__LANG__/?'],
    PREFIX=None,
    SPECIFICATION_PATH='konferencije'
)
@authenticated(
    authentication_level='WEAK',
)
class Index(Base):
    @params(
        {'name': 'language', 'type': str, 'doc': 'language', 'required': False, 'default': 'en'},
        {'name': 'offset', 'type': int, 'doc': 'language', 'required': False, 'default': 0},
        {'name': 'limit', 'type': int, 'doc': 'language', 'required': False, 'default': 100},

    )
    @readonly()
    @db()
    def get(self, lang, offset, limit, **kwargs):
        ulang = '/' + lang + '/' if lang != 'en' and lang else '/'

        posts = self.orm_session.query(SitePage).all()

        self.render('templates/index.html', ulang=ulang, lang=lang,
                    user=self.auth_user is not None, tr=translate,
                    languages=languages, url='/',posts=posts,
                    # user=self.auth_user.user if self.auth_user else None
                    )


@api(
    URI=['/registration/?', '/:__LANG__/registration/?'],
    PREFIX=None,
    SPECIFICATION_PATH='konferencije'
)
@authenticated(
    authentication_level='WEAK',
)
class Registration(Base):
    @params(
        {'name': 'language', 'type': str, 'doc': 'language', 'required': False, 'default': 'en'},
    )
    @readonly()
    @db()
    def get(self, lang, **kwargs):
        ulang = '/' + lang + '/' if lang != 'en' and lang else '/'

        self.render('templates/pages/user/registration.html',
                    languages=languages, url='/registration',
                    lang=lang, ulang=ulang,
                    user=self.auth_user.user if self.auth_user else None)


@api(
    URI=['/login/?', '/:__LANG__/login/?'],
    PREFIX=None,
    SPECIFICATION_PATH='konferencije'
)
@authenticated(
    authentication_level='WEAK',
)
class Login(Base):
    @params(
        {'name': 'language', 'type': str, 'doc': 'language', 'required': False, 'default': 'en'},
    )
    @readonly()
    @db()
    def get(self, lang, **kwargs):
        ulang = '/' + lang + '/' if lang != 'en' and lang else '/'

        self.render('templates/pages/login.html',
                    languages=languages, url='/login',
                    lang=lang, ulang=ulang,
                    user=self.auth_user.user if self.auth_user else None)




@api(
    URI=['/register/?', '/:__LANG__/register/?'],
    PREFIX=None,
    SPECIFICATION_PATH='konferencije'
)
@authenticated(
    authentication_level='WEAK',
)
class Register(Base):
    @params(
        {'name': 'language', 'type': str, 'doc': 'language', 'required': False, 'default': 'en'},
    )
    @readonly()
    @db()
    def get(self, lang, **kwargs):
        ulang = '/' + lang + '/' if lang != 'en' and lang else '/'

        self.render('templates/pages/register.html',
                    languages=languages, url='/register',
                    lang=lang, ulang=ulang,
                    user=self.auth_user.user if self.auth_user else None)



@api(
    URI=['/authors/?', '/:__LANG__/authors/?'],
    PREFIX=None
)
@authenticated(
    authentication_level='WEAK',
)
@db()
class Authors(Base):
    @params(
        {'name': 'lang', 'type': str, 'doc': 'language', 'required': False, 'default': 'sr'},
    )
    def get(self, lang, **kwargs):
        ulang = '/' + lang + '/' if lang != 'sr' and lang else '/'

        page = self.orm_session.query(Post).filter(Post.language == lang).all()
        posts = self.orm_session.query(SitePage).filter(SitePage.language == lang).order_by(
            asc(SitePage.created)).all()

        authors = self.orm_session.query(User).all()

        self.render('templates/pages/authors.html', url='/blog',
                    lang=lang, languages=languages, ulang=ulang,
                    title="Blog", user=self.auth_user is not None,
                    page=page, posts=posts,authors=authors
                    )

@api(
    URI=['/blog/?', '/:__LANG__/blog/?'],
    PREFIX=None
)
@authenticated(
    authentication_level='WEAK',
)
@db()
class Blog(Base):
    @params(
        {'name': 'lang', 'type': str, 'doc': 'language', 'required': False, 'default': 'sr'},
    )
    def get(self, lang, **kwargs):
        ulang = '/' + lang + '/' if lang != 'sr' and lang else '/'

        page = self.orm_session.query(Post).filter(Post.language == lang).all()
        posts = self.orm_session.query(SitePage).filter(SitePage.language == lang).order_by(
            asc(SitePage.created)).all()

        self.render('templates/pages/blog.html', url='/blog',
                    lang=lang, languages=languages, ulang=ulang,
                    title="Blog", user=self.auth_user is not None,
                    page=page, posts=posts,
                    )

@api(
    URI=['/blog/:slug/?', '/:__LANG__/blog/:slug/?'],
    PREFIX=None
)
@authenticated(
    authentication_level='WEAK',
)
class BlogArticle(Base):
    @params(
        {'name': 'slug', 'type': str, 'doc': 'language', 'required': True},
        {'name': 'language', 'type': str, 'doc': 'language', 'required': False, 'default': 'sr'},
        {'name': 'id', 'type': int, 'doc': 'id', 'required': False},
        {'name': 'group', 'type': int, 'doc': 'group', 'required': False},

    )
    @db()
    def get(self, slug, lang, _id, group):
        ulang = '/' + lang + '/' if lang != 'sr' and lang else '/'
        page = self.orm_session.query(SitePage).filter(SitePage.slug == slug).one_or_none()
        # print(page.title)
        if not page.template:
            self.write('missing page template')
            return

        tpl = page.template

        page_posts = self.orm_session.query(Post).filter(Post.page == page).all()
        # print(page_posts)

        a = 'templates/pages/{}.html'.format(tpl.id)
        u = '/blog/{}'.format(page.slug)

        dirname = os.path.dirname(__file__)
        loader = Loader(f'{dirname}/templates/')
        b = dirname + '/' + a
        try:
            with open(b, 'rt') as f:
                t = Template(
                    f.read(), loader=loader
                )
        except Exception as e:
            print(e)


        print(self.auth_user)

        try:
            h = t.generate(url=u, page_posts=page_posts, user=self.auth_user if self.auth_user else None,
                    lang=lang, ulang=ulang, page=page,  template=tpl)
            self.write(h)
        except Exception as e:
            print(e)
        a = 3



# @api(
#     URI=['/news/news_article/?', '/:__LANG__/news/news_article/?'],
#     PREFIX=None,
#     SPECIFICATION_PATH='konferencije'
# )
# @authenticated(
#     authentication_level='WEAK'
# )
# class NewsArticle(Base):
#     @params(
#         {'name': 'language', 'type': str, 'doc': 'language', 'required': False, 'default': 'en'},
#     )
#     @readonly()
#     @db()
#     def get(self, lang, **kwargs):
#         ulang = '/' + lang + '/' if lang != 'en' and lang else '/'
#
#         posts = [p for p in self.orm_session.query(blog.Post).all()]
#
#         # posts = [p for p in
#         #             self.orm_session.query(models.Post).join(models.ProductTranslatedData).all()]
#
#         self.render('templates/pages/news_article.html', title=translate(lang, 'news'),
#                     languages=languages, url='', tr=translate, lang=lang,
#                     ulang=ulang,
#                     user=self.auth_user.user if self.auth_user else None)


# @api(
#     URI=['/news/?', '/:__LANG__/news/?'],
#     PREFIX=None,
#     SPECIFICATION_PATH='konferencije'
# )
# @authenticated(
#     authentication_level='WEAK'
# )
# class Posts(Base):
#     @params(
#         {'name': 'language', 'type': str, 'doc': 'language', 'required': False, 'default': 'en'},
#         {'name': 'slug', 'type': str, 'required': False, 'default': 'en'}
#     )
#     @readonly()
#     @db()
#     def get(self, lang, slug, **kwargs):
#         ulang = '/' + lang + '/' if lang != 'en' and lang else '/'
#         #
#         # posts = [p for p in self.orm_session.query(blog.Post).filter(blog.Post.published_time != None,
#         #                                                              blog.Post.removed == False).all()]
#         #
#         # unpublished_posts = [p for p in self.orm_session.query(blog.Post).filter(blog.Post.published_time == None,
#         #                                                                          blog.Post.removed == False).all()]
#
#         post = self.orm_session.query(blog.Post).filter(blog.Post.slug == slug).one_or_none()
#
#         self.render('templates/pages/news.html',
#                     languages=languages, url='', lang=lang, post=post, slug=slug,
#                     ulang=ulang,
#                     user=self.auth_user.user if self.auth_user else None)
#
#
# @api(
#     URI=['/news/:slug/?', '/:__LANG__/news/:slug/?'],
#     PREFIX=None,
#     SPECIFICATION_PATH='konferencije'
# )
# @authenticated(
#     authentication_level='WEAK'
# )
# class Post(Base):
#     @params(
#         {'name': 'language', 'type': str, 'doc': 'language', 'required': False, 'default': 'en'},
#         {'name': 'slug', 'type': str, 'required': False, 'default': 'en'},
#         {'name': 'id_post', 'type': str, 'required': False, 'default': 'en'},
#     )
#     @readonly()
#     @db()
#     def get(self, lang, slug, id_post, **kwargs):
#         ulang = '/' + lang + '/' if lang != 'en' and lang else '/'
#
#         post = self.orm_session.query(blog.Post).filter(blog.Post.slug == slug).one_or_none()
#         comment = self.orm_session.query(blog.Post).filter(blog.Post.slug == slug).one_or_none()
#
#         comments = self.orm_session.query(blog.Comment).all()
#         posts = [p for p in self.orm_session.query(blog.Post).all()]
#         # comments = [p for p in self.orm_session.query(blog.Comment).all()]
#
#         self.render('templates/pages/news_article.html',
#                     languages=languages, url=f'/news/{slug}', lang=lang,
#                     ulang=ulang, posts=posts, post=post, comments=comments,
#                     comment=comment,
#                     user=self.auth_user.user if self.auth_user else None)
#
#
# @api(
#     URI=['/news/add_news/?', '/:__LANG__/news/add_news/?'],
#     PREFIX=None,
#     SPECIFICATION_PATH='konferencije'
# )
# @authenticated(
#     # authentication_level='WEAK'
#     redirect_url='/login'
# )
# class AddNews(Base):
#     @params(
#         {'name': 'language', 'type': str, 'doc': 'language', 'required': False, 'default': 'en'}
#     )
#     @readonly()
#     @db()
#     def get(self, lang, **kwargs):
#         ulang = '/' + lang + '/' if lang != 'en' and lang else '/'
#
#         self.render('templates/pages/news_add.html', title='Add news',
#                     languages=languages, url='',lang=lang,
#                     ulang=ulang,
#                     user=self.auth_user.user if self.auth_user else None)
#
#
# @api(
#     URI=['/courses/?', '/:__LANG__/courses/?'],
#     PREFIX=None,
#     SPECIFICATION_PATH='konferencije'
# )
# @authenticated(
#     authentication_level='WEAK'
# )
# class Courses(Base):
#     @params(
#         {'name': 'language', 'type': str, 'doc': 'language', 'required': False, 'default': 'en'}
#     )
#     @readonly()
#     @db()
#     def get(self, lang, **kwargs):
#         ulang = '/' + lang + '/' if lang != 'en' and lang else '/'
#
#         self.render('templates/pages/courses.html',
#                     languages=languages, url='/courses', lang=lang,
#                     ulang=ulang,
#                     user=self.auth_user.user if self.auth_user else None)
#
#
# @api(
#     URI=['/courses/course/?', '/:__LANG__/courses/course/?'],
#     PREFIX=None,
#     SPECIFICATION_PATH='konferencije'
# )
# @authenticated(
#     authentication_level='WEAK'
# )
# class Course(Base):
#     @params(
#         {'name': 'language', 'type': str, 'doc': 'language', 'required': False, 'default': 'en'}
#     )
#     @readonly()
#     @db()
#     def get(self, lang, **kwargs):
#         ulang = '/' + lang + '/' if lang != 'en' and lang else '/'
#
#         self.render('templates/pages/course.html',
#                     languages=languages, url='',  lang=lang,
#                     ulang=ulang,
#                     user=self.auth_user.user if self.auth_user else None)
#



# from base.application.components import Base
# from base.application.components import api
# from base.application.components import params
# from base.application.components import authenticated, db, readonly
# from web_blog.translations import translate
# from src.config.app_config import default_lang
# import src.models.blog as mblog
# import src.models.site as site
# from sqlalchemy import desc, asc
# import src.models.site as smodels
#
# languages = [
#     {'id': 'de', 'text': 'german'},
#     {'id': 'it', 'text': 'italian'},
#     {'id': 'en', 'text': 'english'}
# ]
#
# """
# LOGIN PAGE
# """
#
#
# @api(
#     URI=['/admin/?', '/:__LANG__/admin/?'],
#     PREFIX=None
# )
# @authenticated(
#     authentication_level='WEAK',
# )
# class Login(Base):
#     @params(
#         {'name': 'language', 'type': str, 'doc': 'language', 'required': False, 'default': default_lang},
#     )
#     def get(self, lang, **kwargs):
#         ulang = '/' + lang + '/' if lang != default_lang and lang else '/'
#         logged_in = self.auth_user is not None
#
#         if logged_in:
#             self.redirect(lang + '/')
#         page_group = ''
#         self.render('templates/pages/admin-login.html', url='/admin',
#                     logged_in=logged_in, page_group=page_group,
#                     lang=lang, tr=translate, languages=languages, ulang=ulang)
#
#
# """
# ALL STATIC PAGES ON WEBSITE
# """
#
#
# @api(
#     URI=['/?', '/:__LANG__/?'],
#     PREFIX=None
# )
# @authenticated(
#     authentication_level='WEAK',
# )
# class Index(Base):
#     @params(
#         {'name': 'language', 'type': str, 'doc': 'language', 'required': False, 'default': default_lang},
#         {'name': 'slug', 'type': str, 'doc': 'slug', 'required': False},
#     )
#     @readonly()
#     @db()
#     def get(self, lang, slug, **kwargs):
#         ulang = '/' + lang + '/' if lang != default_lang and lang else '/'
#
#         self.render('templates/index.html', url='/')
#
