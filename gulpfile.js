const gulp = require('gulp'),
      concat = require("gulp-concat"),
      sass = require('gulp-sass')
;

gulp.task('js', function () {
  return gulp.src([
    // 'static/js/modules/api/user/register.js',
    'frontend/js/req.js',
    'frontend/js/api/user/signin.js',
    'frontend/js/api/user/signout.js',
    'frontend/js/api/template_create.js',
    'frontend/js/functionality/edit_mode.js',
    'frontend/js/components/slider_config.js',
    // 'static/js/modules/api/comments_add.js',
    // 'static/js/modules/api/comments_add.js',
    // 'static/js/modules/api/news.js',
    // 'static/js/modules/api/post_add.js',
    // 'static/js/modules/api/post_delete.js',
    // 'static/js/modules/api/post_edit.js',
    // 'static/js/modules/api/post_publish.js',
    //
    // 'static/js/modules/components/accordion.js',
    'frontend/js/components/cropper.js',
    // 'static/js/modules/components/dropdowns.js',
    // 'static/js/modules/components/modals.js',

    //
    // // 'static/js/modules/functionality/set_active_link.js',
    'frontend/js//functionality/social_share.js',

  ])
    .pipe(concat('bundle.js'))
    .pipe(gulp.dest('static/js'))
});

gulp.task('sass', function () {
  return gulp.src('./frontend/scss/styles.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./static/css'))
});

gulp.task('sass:watch', function() {
  gulp.watch('./frontend/scss/**/*.scss', gulp.series('sass'));
});

gulp.task('js:watch', function() {
  gulp.watch(['./frontend/js/**/*.js'], gulp.series('js'));
});


gulp.task('default', gulp.parallel(['js', 'sass:watch', 'js:watch']));

gulp.task('build', gulp.parallel(['js', 'sass']));





