# coding= utf-8
import json

from base.common.utils import log
from base.application.components import Base
from base.application.components import api
from base.application.components import params
from base.application.components import authenticated

from src.config.app_config import default_lang
from src.models.site import SitePage, SiteTemplate, SiteTemplateSection
import src.models.site as smodels
from src.models.blog import TranslatedData, Post
from base.common.utils import log

import random

import json
import datetime


# @authenticated()
@api(URI='/ping')
class GetPing(Base):
    @params(
        {'name': 'address', 'type': str, 'required': False},
    )
    def get(self, address):
        pong = str(datetime.datetime.now())
        return self.ok({"pong": pong})


@authenticated()
@api(
    URI='/site/page',
    SPECIFICATION_PATH='Site'
)
class Page(Base):

    @params(
        {'name': 'url', 'type': str, 'doc': 'page url', 'required': True},
    )
    def get(self, page_url):
        import base.common.orm
        OrmPage, _session = base.common.orm.get_orm_model('site_page')
        _page = _session.query(OrmPage).filter(OrmPage.url == page_url).one_or_none()

        _page_meta = '' if _page is None else json.loads(_page.html_meta)['html_meta']

        return self.ok({'page_meta': _page_meta})

    @params(
        {'name': 'url', 'type': str, 'doc': 'page url', 'required': True},
        {'name': 'html_meta', 'type': json, 'doc': 'page meta', 'required': True}
    )
    def put(self, page_url, html_meta):
        import base.common.orm
        OrmPage, _session = base.common.orm.get_orm_model('site_page')
        _page = _session.query(OrmPage).filter(OrmPage.url == page_url).one_or_none()

        try:
            _html_meta = json.dumps({'html_meta': html_meta})
        except Exception as e:
            log.critical('Error save data; {}'.format(e))
            return self.error('Error save page data')

        if _page is None:
            _page = OrmPage(page_url, _html_meta)
            _session.add(_page)
        else:
            _page.html_meta = _html_meta

        _session.commit()

        return self.ok({'id': _page.id})


@authenticated()
@api(
    URI='/site/template',
    SPECIFICATION_PATH='Site'
)
class Template(Base):
    def get(self):
        return self.ok({'todo': True})

    @params(
        {'name': 'id', 'type': str, 'doc': 'tpl id', 'required': True},
        {'name': 'name', 'type': str, 'doc': 'tpl Name', 'required': True},
        {'name': 'structure', 'type': json, 'doc': 'tpl structure', 'required': False}
    )
    def put(self, id, name, structure):

        template = SiteTemplate(id, name)
        self.orm_session.add(template)

        if structure:
            if 'sections' in structure:
                for s in structure['sections']:
                    sec = SiteTemplateSection(template, s['id'], s['name'] if 'name' in s else s['id'])
                    self.orm_session.add(sec)

        try:
            self.orm_session.commit()
            log.info('user {} - added new template in system.'.format(self.auth_user.username))

        except:
            self.orm_session.rollback()
            log.warning('error adding new template in system.')
            return self.error("Error adding template")

        return self.ok({'id': template.id})


@authenticated()
@api(
    URI='/site/template/:id_template/pages',
    SPECIFICATION_PATH='Site'
)
class TemplatePage(Base):
    def get(self):
        return self.ok({'todo': True})

    @params(
        {'name': 'id_template', 'type': str, 'doc': 'tpl Name', 'required': True},
        {'name': 'title', 'type': str, 'doc': 'tpl title', 'required': True},
        {'name': 'body', 'type': str, 'doc': 'tpl body', 'required': True},
        {'name': 'slug', 'type': str, 'doc': 'tpl slug', 'required': True},
        {'name': 'language', 'type': str, 'doc': 'tpl slug', 'required': True},
    )
    def put(self, id_template, title, body, slug, language):
        import os
        from tornado.template import Template, Loader
        template = self.orm_session.query(SiteTemplate).filter(SiteTemplate.id == id_template).one_or_none()

        selected_template = '/../../../web_blog/templates/pages/template1.html'
        number_of_posts = 3

        if template:
            dirname = os.path.dirname(__file__)
            loader = Loader(f'{dirname}/../../../web_blog/templates/')
            with open(os.path.abspath(os.path.dirname(__file__)) + selected_template) as f:
                t = Template(
                    f.read(), loader=loader
                )

        else:
            return self.error('tpl not found')

        groups_in_db = self.orm_session.query(SitePage.group_pages).all()
        import random
        not_found = True
        group_random = 0
        while not_found:
            group_random = random.randrange(0, 100000000)
            if not group_random in groups_in_db:
                not_found = False

        p1 = SitePage(template, title, slug, language, group_random, True)
        print(p1.title, p1.body, p1.slug, p1.language)
        self.orm_session.add(p1)

        try:
            self.orm_session.commit()
            log.info('user - add new page in system.')
        except Exception as e:
            self.orm_session.rollback()
            log.warning('user  - error adding new page in system.')
            return self.error("Error adding page === {}".format(e))

        for page in range(number_of_posts):
            post1 = Post(self.auth_user.user, title, 'subtitle', body, random.randint(1000, 999999999999), 0, '', '',
                         'sr', p1)
            self.orm_session.add(post1)
            self.orm_session.add(TranslatedData('sr', True,
                                                {'title': 'Lorem Ipsum',
                                                 'subtitle': 'Duis aute irure dolor in reprehenderit ',
                                                 'body': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat'},
                                                post1))

        try:
            self.orm_session.commit()
            log.info('user {} - add new page in system.'.format(self.auth_user.username))
        except Exception as e:
            self.orm_session.rollback()
            log.warning('user {} - error adding new page in system.'.format(self.auth_user.username))
            return self.error("Error adding posts to page ---- {}".format(e))

        return self.ok({'id1': p1.id})

    @params(
        {'name': 'id', 'type': str, 'doc': 'id', 'required': True},
    )
    def delete(self, _id):
        query = self.orm_session.query(SitePage).filter(SitePage.id == _id)
        page = query.one_or_none()
        if not page:
            return self.error('page not found')
        else:
            page.status = False
            print(page.status)
            self.orm_session.commit()
            return self.ok()

    @params(
        {'name': 'id', 'type': int, 'doc': 'page id', 'required': True},
        {'name': 'title', 'type': str, 'doc': 'page title', 'required': False},
        {'name': 'body', 'type': str, 'doc': 'page body', 'required': False},
        {'name': 'lang', 'type': str, 'doc': 'page lang', 'required': True},
        {'name': 'slug', 'type': str, 'doc': 'page slug', 'required': False}
    )
    def patch(self, _id, title, body, lang, slug):
        _session = self.orm_session
        page = _session.query(SitePage).filter(SitePage.id == _id, SitePage.language == lang).one_or_none()
        if not page:
            return self.error('Page not found')

        if title:
            if page.title != title:
                page.title = title
        if body:
            if page.body != body:
                page.body = body
        if slug:
            if page.slug != slug:
                page.slug = slug

        try:
            _session.commit()
            log.info('Changed page info')
        except Exception as e:
            self.orm_session.rollback()
            log.warning('user {} - error editing page title and body'.format(self.auth_user.username))
            return self.error("Error editing page title and body ---- {}".format(e))

        return self.ok()


@authenticated()
@api(
    URI='/site/publish-page/:id',
    SPECIFICATION_PATH='Site'
)
class CheckPage(Base):
    @params(
        {'name': 'id', 'type': str, 'doc': 'id', 'required': True},
        {'name': 'published', 'type': bool, 'doc': 'lang', 'required': True},
        {'name': 'lang', 'type': str, 'doc': 'lang', 'required': True},
        {'name': 'group', 'type': str, 'doc': 'lang', 'required': True},
    )
    def patch(self, _id, published, lang, group):
        # published = False

        page = self.orm_session.query(SitePage).filter(SitePage.group_pages == group,
                                                       SitePage.language == lang).one_or_none()
        print(page.language)
        if page:
            page.status = published
            print(page.status)

        try:
            self.orm_session.commit()
            log.info('Changed page status')
        except Exception as e:
            self.orm_session.rollback()
            log.warning('user {} - error changing page status'.format(self.auth_user.username))
            return self.error("Error changing page status ---- {}".format(e))

        return self.ok()
