# coding= utf-8

import json
import datetime
import os
import sqlalchemy.exc

from base.common.utils import log
from base.application.components import Base
from base.application.components import api
from base.application.components import params
from base.application.components import authenticated

from src.models.blog import Post, Tag, ShowTag, TranslatedData
from src.common.common import get_comments
from src.common.common import get_post_files
import base64
import hashlib
import tornado.gen
from tempfile import NamedTemporaryFile
from PIL import Image as PILImage
import shutil
from src.config import blog_config as bc
import src.models.blog as tmodels
from src.config.app_config import default_lang


@authenticated()
@api(
    URI='/wiki/posts',
    SPECIFICATION_PATH='Blog'
)
class AddPost(Base):
    @params(  # if you want to add params
        {'name': 'title', 'type': str, 'doc': 'title', 'required': True},
        {'name': 'subtitle', 'type': str, 'doc': 'subtitle', 'required': False},
        {'name': 'body', 'type': str, 'doc': 'body', 'required': False},
        {'name': 'category', 'type': str, 'doc': 'category', 'required': False},
        {'name': 'tags', 'type': list, 'doc': 'list of tags', 'required': False},
        {'name': 'slug', 'type': str, 'doc': 'slug', 'required': False},
        {'name': 'enable_comments', 'type': bool, 'doc': 'enable comments', 'required': False, 'default': True},
        {'name': 'only_authorized_comments', 'type': bool, 'doc': 'only authorized comments', 'required': False,
         'default': False},
        {'name': 'source', 'type': str, 'doc': 'source', 'required': False, 'default': None},
        {'name': 'datetime', 'type': datetime.datetime, 'doc': 'datetime', 'required': False, 'default': None},
        {'name': 'cover_img', 'type': str, 'doc': 'cover0img', 'required': False},
        {'name': 'tumb_img', 'type': str, 'doc': 'cover0img', 'required': False},
        {'name': 'language', 'type': str, 'doc': 'post language', 'required': False, 'min': 2, 'max': 2,
         'lowercase': True, 'default': default_lang},
        {'name': 'youtube_link', 'type': str, 'doc': 'youtube short code', 'required': False},
        {'name': 'id_group', 'type': int, 'doc': 'group id', 'required': False},
        {'name': 'html_meta', 'type': json, 'doc': 'post html meta tags', 'required': False},
        # {'name': 'translations', 'type': json, 'doc': 'translations', 'required': False},
        {'name': 'id_page', 'type': int, 'required': False},
        {'name': 'id_template_section', 'type': str, 'required': False},
        {'name': 'tpl_section_order', 'type': int, 'required': False},
        {'name': 'id_post', 'type': str, 'required': False}
    )
    def put(self, title, subtitle, body, category, tags, slug, enable_comments, only_authorized_comments, source,
            forced_datetime, cover_img, tumb_img, language, youtube_link, id_group, html_meta,
            # translations,
            id_page, id_template_section, tpl_section_order, id_post

            ):
        import base.common.orm
        _session = self.orm_session
        _slug = Post.mkslug(title) if not slug else Post.mkslug(slug)

        _group = None
        if id_group is not None:
            _g = _session.query(Post).filter(Post.id == id_group).one_or_none()
            if _g is not None:
                _group = _g

        _html_meta = None
        if html_meta is not None:
            try:
                _html_meta = json.dumps({
                    'html_meta': html_meta
                }, ensure_ascii=False)
            except Exception as e:
                log.critical('Can not save meta: {}'.format(e))
                return self.error('Error add post')

        if id_post:
            p = self.orm_session.query(Post).filter(Post.id == id_post).one_or_none()
            if not p:
                log.error('Post {} not found'.format(id_post))
                return self.error('Post not found')
        else:
            try:
                p = Post(self.auth_user.user, title, subtitle, body, _slug, tags, cover_img, tumb_img, language,
                         enable_comments=enable_comments,
                         only_authorized_comments=only_authorized_comments,
                         source=source,
                         forced_datetime=forced_datetime,
                         str_category=category,
                         youtube_link=youtube_link,
                         group=_group,
                         html_meta=_html_meta)
            except sqlalchemy.exc.IntegrityError as e:
                log.critical('Can not add new post: {}'.format(e))
                if e.orig is not None and e.orig.args is not None:
                    if e.orig.args[0] == 1062 and 'post_id_group_lang_ux_1' in e.orig.args[1]:
                        return self.error('Can not add post with the same group and language')
                return self.error('Error add post')

        translation = self.orm_session.query(TranslatedData).filter(TranslatedData.post == p,
                                                                    TranslatedData.language == language).one_or_none()
        if translation:
            import copy
            _translation_data = copy.deepcopy(translation.data) if translation.data else {}
            if 'title' not in _translation_data or _translation_data['title'] != title:
                _translation_data['title'] = title
            if 'subtitle' not in _translation_data or _translation_data['subtitle'] != subtitle:
                _translation_data['subtitle'] = subtitle
            if 'body' not in _translation_data or _translation_data['body'] != body:
                _translation_data['body'] = body
            if 'slug' not in _translation_data or _translation_data['slug'] != _slug:
                _translation_data['slug'] = _slug
            translation.data = _translation_data
        else:
            _translate_data = {
                'title': title,
                'subtitle': subtitle,
                'body': body,
                'slug': _slug
            }
            translation = TranslatedData(language, language == default_lang, _translate_data, p)
        # id_page, id_template_section, tpl_section_order
        if id_page:
            p.id_page = id_page

            if not id_template_section:
                _session.rollback()
                return self.error('missing id_template_section')

            p.id_template_section = id_template_section

            if tpl_section_order:
                p.tpl_section_order = tpl_section_order
            else:
                import sqlalchemy
                max_oid = _session.query(sqlalchemy.func.max(Post.tpl_section_order)).filter(
                    Post.id_page == id_page,
                    Post.id_template_section == id_template_section
                ).one_or_none()
                if max_oid is None or max_oid[0] is None:
                    max_oid = (0,)

                p.tpl_section_order = max_oid[0] + 1

        _session.add(p)

        _session.commit()
        log.info('new post is created by {}'.format(self.auth_user.username))
        return self.ok({'id': p.id, 'slug': p.slug})

    def get(self, language):

        posts = []
        all_posts = self.orm_session.query(tmodels.Post).all()

        if self.auth_user.user.posts:
            posts = [p.title for p in all_posts]

        # for p in posts:
        #     print(p)

        # print(posts)
        return self.ok({
            'posts': posts
        })

    # @params(
    #     {'name': 'language', 'type': str, 'doc': 'language', 'required': True},
    #     {'name': 'id_post', 'type': str, 'doc': 'id_post', 'required': False}
    # )
    # def get(self, language, id_post):
    #
    #     posts = []
    #     if self.auth_user.user.posts:
    #         posts = [p.language for p in self.auth_user.user.posts]
    #
    #     if not language or not id_post:
    #         return self.error('not found')
    #
    #     page = self.orm_session.query(Post).join(tmodels.TranslatedData).filter(
    #         tmodels.TranslatedData.language == language, Post.id == id_post).one_or_none()
    #
    #     if not page:
    #         return self.error('page not found')
    #
    #     return self.ok({
    #
    #     })


@authenticated()
@api(
    URI='/wiki/check-slug',
    SPECIFICATION_PATH='Blog'
)
class CheckSlugPost(Base):
    @params(
        {'name': 'slug', 'type': str, 'doc': 'slug', 'required': True}
    )
    def get(self, slug):
        _slug = Post.mkslug(slug)

        return self.ok({
            'slug': _slug
        })


@authenticated()
@api(
    URI='/wiki/posts/tagged_with/:tag',
    SPECIFICATION_PATH='Blog'
)
class PostsByTag(Base):
    @params(
        {'name': 'tag', 'type': str, 'doc': 'id', 'required': True}
    )
    def get(self, tag):

        import base.common.orm
        _session = self.orm_session

        db_tag = _session.query(Tag).filter(Tag.name == Tag.tagify(tag)).one_or_none()
        if not db_tag:
            return self.error('tag not found')

        posts = []
        for post in db_tag.posts:
            posts.append({
                'id': post.id,
                'slug': post.slug,
                'author': {
                    'email': post.user.auth_user.username,
                    'first_name': post.user.first_name,
                    'last_name': post.user.last_name
                },
                'title': post.title,
                'created_datetime': str(post.created),
                'updated_datetime': str(post.last_modified_datetime),
                'status': post.id_status
            })

        return self.ok({'posts': posts})


@authenticated()
@api(
    URI='/wiki/posts/:id/tags',
    SPECIFICATION_PATH='Blog'
)
class PostTags(Base):
    @params(
        {'name': 'id', 'type': int, 'doc': 'id', 'required': True}
    )
    def get(self, _id):
        import base.common.orm
        from src.models.blog import Post
        _session = self.orm_session

        p = _session.query(Post).filter(Post.id == _id).one_or_none()
        if not p:
            return self.error("Post not found")

        return self.ok({'tags': [t.name for t in p.show_tags]})

    @params(
        {'name': 'id', 'type': int, 'doc': 'id', 'required': True},
        {'name': 'tag_name', 'type': str, 'doc': 'id', 'required': True}
    )
    def put(self, _id, tag_name):
        import base.common.orm
        from src.models.blog import Post
        _session = self.orm_session

        p = _session.query(Post).filter(Post.id == _id).one_or_none()
        if not p:
            return self.error("Post not found")

        added = p.tagg_it([tag_name])

        if added > 0:
            _session.commit()

        if added == 0:
            return self.error('tag already exists')

        return self.ok({'added': added})


@authenticated()
@api(
    URI='/wiki/posts/:id/tags/:show_tag_name',
    SPECIFICATION_PATH='Blog'
)
class PostTagDeleteHandler(Base):
    @params(
        {'name': 'id', 'type': int, 'doc': 'id', 'required': True},
        {'name': 'show_tag_name', 'type': str, 'doc': 'id', 'required': True}
    )
    def delete(self, _id, show_tag_name):
        import base.common.orm
        from src.models.blog import Post
        _session = self.orm_session

        p = _session.query(Post).filter(Post.id == _id).one_or_none()
        if not p:
            return self.error("Post not found")

        show_tag = _session.query(ShowTag).filter(ShowTag.name == show_tag_name).one_or_none()
        if not show_tag:
            return self.error('tag not found')

        p.show_tags.remove(show_tag)

        tag = _session.query(Tag).filter(Tag.name == Tag.tagify(show_tag_name)).one_or_none()
        if tag:
            p.tags.remove(tag)

        _session.commit()

        return self.ok()


@authenticated()
@api(
    URI='/wiki/posts/:id',
    SPECIFICATION_PATH='Blog'
)
class PostById(Base):
    @params(
        {'name': 'id', 'type': int, 'doc': 'id', 'required': True}
    )
    def get(self, _id):

        import base.common.orm
        from src.models.blog import Post
        _session = self.orm_session

        p = _session.query(Post).filter(Post.id == _id).one_or_none()
        if not p:
            return self.error("Post not found")

        try:
            _body = json.loads(p.body)
        except:
            _body = ''

        try:
            _html_meta = json.loads(p.html_meta)
            _html_meta = json.dumps(_html_meta['html_meta'])
        except:
            _html_meta = ''

        return self.ok({
            'author': {
                'email': p.user.auth_user.username,
                'first_name': p.user.first_name,
                'last_name': p.user.last_name
            },
            'title': p.title,
            'body': _body,
            'tags': [t.name for t in p.show_tags],
            'attached_files': get_post_files(p),
            'comments': get_comments(p.id, canonical=True),
            'html_meta': _html_meta
        })

    @params(
        {'name': 'id', 'type': int, 'doc': 'id', 'required': True},
        {'name': 'title', 'type': str, 'doc': 'title', 'required': False},
        {'name': 'subtitle', 'type': str, 'doc': 'title', 'required': False},
        {'name': 'body', 'type': str, 'doc': 'body', 'required': False},
        {'name': 'tags', 'type': list, 'doc': 'tags', 'required': False, 'default': None},
        {'name': 'category', 'type': str, 'doc': 'category', 'required': False},
        {'name': 'comments_enabled', 'type': bool, 'doc': 'comments_enabled status of post', 'required': False,
         'default': True},
        {'name': 'status', 'type': int, 'doc': 'status of post', 'required': False},
        {'name': 'html_meta', 'type': json, 'doc': 'post html meta tags', 'required': False},
        {'name': 'youtube_link', 'type': str, 'doc': 'youtube link', 'required': False},
        {'name': 'tumb_img', 'type': str, 'doc': 'cover0img', 'required': False},
        {'name': 'cover_img', 'type': str, 'doc': 'cover0img', 'required': False},
        # {'name': 'translations', 'type': json, 'doc': 'translations', 'required': False},
        {'name': 'language', 'type': str, 'doc': 'post language', 'required': False, 'min': 2, 'max': 2,
         'lowercase': True, 'default': default_lang},

    )
    def patch(self, _id, title, subtitle, body, tags, category, comments_enabled, status, html_meta, youtube_link,
              tumb_img, cover_img, language):

        import base.common.orm
        from src.models.blog import Post
        from sqlalchemy import update
        _session = self.orm_session

        p = _session.query(Post).filter(Post.id == _id).one_or_none()
        t = _session.query(TranslatedData).filter(TranslatedData.post == p,
                                                  TranslatedData.language == language).one_or_none()
        print(t)

        if not p:
            return self.error("Post not found")

        temp = {
            'title': title,
            'subtitle': subtitle,
            'body': body
        }
        t.data = temp
        # print(t.data)
        # stmt = TranslatedData.update(t).where(t.post == p, t.language == language).values(data=temp)
        _session.add(t)
        _session.commit()


        import src.lookup.post_status as post_status

        # if not p.published_time and status == post_status.PUBLISHED:
        #     p.published_time = datetime.datetime.now()

        # try:
        #     _body = json.loads(t.body)
        # except:
        #     _body = ''

        # _html_meta = None
        # if html_meta is not None:
        #     try:
        #         _html_meta = json.dumps({
        #             'html_meta': html_meta
        #         }, ensure_ascii=False)
        #     except Exception as e:
        #         log.critical('Can not save meta: {}'.format(e))
        #         return self.error('Error update post')

        # translation = TranslatedData('de', language == default_lang, {'title': 'sadfasd', 'subtitle': 'asdfasdf', 'body': 'adfasdfasdf'}, p)
        log.info('post with id {} is changed by {}'.format(_id,self.auth_user.username))
        return self.ok({'changed': "changed"})


@authenticated()
@api(
    URI='/wiki/posts/group/:id_group',
    SPECIFICATION_PATH='Blog'
)
class PostGroupById(Base):
    @params(
        {'name': 'id_group', 'type': int, 'doc': 'post group id', 'required': True}
    )
    def get(self, _id_group):

        import base.common.orm
        from src.models.blog import Post
        _session = self.orm_session

        p = _session.query(Post).filter(Post.id_group == _id_group)
        _posts = []
        for _p in p:

            try:
                _body = json.loads(_p.body)
            except json.JSONDecodeError:
                _body = ''

            _posts.append({
                'author': {
                    'email': _p.user.auth_user.username,
                    'first_name': _p.user.first_name,
                    'last_name': _p.user.last_name
                },
                'title': _p.title,
                'body': _body,
                'tags': [t.name for t in _p.show_tags],
                'attached_files': get_post_files(_p),
                'comments': get_comments(_p.id, canonical=True)
            })

        return self.ok({'posts': _posts})


@authenticated()
@api(
    URI='/wiki/diff-lang-posts/:lang',
    SPECIFICATION_PATH='Blog'
)
class PostsOnDifferentLanguage(Base):
    """
    Get all group head posts on different languages
    to be able to connect current working post to some group
    """

    @params(
        {'name': 'lang', 'type': str, 'doc': 'category', 'required': True},
        {'name': 'category', 'type': str, 'doc': 'category', 'required': False}
    )
    def get(self, lang, category):

        import base.common.orm
        from src.models.blog import Post
        _session = self.orm_session

        if category is None:
            posts = _session.query(Post).filter(Post.id_group == Post.id, Post.language != lang).all()
        else:
            posts = _session.query(Post).filter(Post.id_group == Post.id, Post.language != lang,
                                                Post.category == category).all()

        _posts = []
        for p in posts:
            _posts.append({'title': p.title, 'id_group': p.id_group})

        return self.ok({'posts': _posts})


@authenticated()
@api(
    URI='/upload-blog-image/:id_post'
)
class HandleImageUploadForBlog(Base):
    @authenticated()
    @params(
        {'name': 'id_post', 'type': int, 'doc': 'profile image', 'required': True},
        {'name': 'image', 'type': str, 'doc': 'profile image', 'required': True},
        {'name': 'original_name', 'type': str, 'doc': 'profile image', 'required': False, 'default': "untitled"},
    )
    @tornado.gen.coroutine
    def post(self, id_post, base64_image_content, original_name):

        from src.models.blog import Post

        the_post = self.orm_session.query(Post).filter(Post.id == id_post).one_or_none()
        if not the_post:
            yield self.error('post not found')
            return

        _, img = base64_image_content.split(',')

        img = base64.b64decode(img)

        filesize = len(img)

        s = hashlib.sha256()
        s.update(img)

        f = NamedTemporaryFile(delete=False)
        f.write(img)
        f.close()

        img = PILImage.open(f.name)

        target_fname = '{}.{}'.format(s.hexdigest(), img.format).lower()

        shutil.copyfile(f.name, bc.blog_files_directory + target_fname)
        os.unlink(f.name)

        updated = False

        photo = {'fname': target_fname,
                 'width': img.size[0],
                 'height': img.size[1]}

        db_img = self.orm_session.query(tmodels.Image).filter(
            tmodels.Image.user == self.auth_user.user,
            tmodels.Image.unique_filename == photo['fname']
        ).one_or_none()

        if not db_img:
            db_img = tmodels.Image(self.auth_user.user, photo['fname'], filesize, img.size[0], img.size[1],
                                   original_name)
            self.orm_session.add(db_img)
            updated = True

        if not the_post.cover_img or the_post.cover_img != target_fname:
            the_post.cover_img = target_fname
            updated = True

        if updated:
            try:
                self.orm_session.commit()

            except Exception as e:
                yield self.error('excetpion {}'.format(e))
                return

        yield self.ok(photo)
