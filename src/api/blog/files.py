# coding= utf-8

from base.application.components import Base
from base.application.components import api
from base.application.components import params
from base.application.components import authenticated, db

import os
import tornado.gen
import tornado.web

from src.models.blog import PostFile, Image

from src.common.common import get_post_files
import tornado.concurrent as concurrent

import base64
import hashlib
from tempfile import NamedTemporaryFile
from PIL import Image as PILImage
import shutil
from os.path import expanduser
import json
from tornado.template import Template
from base.common.utils import log
import socket

@authenticated()
@api(
    URI='/wiki/posts/:id/files',
    SPECIFICATION_PATH='Blog'
)
class Files(Base):
    """Manipulate files for a post"""

    @params(
        {'name': 'id', 'type': str, 'doc': 'id', 'required': True},
    )
    def get(self, _id):
        import base.common.orm
        from src.models.blog import Post
        _session = self.orm_session

        p = _session.query(Post).filter(Post.id == _id).one_or_none()
        if not p:
            return self.error("Post not found")

        return self.ok({"files": get_post_files(p)})

    @params(
        {'name': 'id', 'type': str, 'doc': 'id', 'required': True},
        {'name': 'filename', 'type': str, 'doc': 'title', 'required': True},
        {'name': 'local_name', 'type': str, 'doc': 'local_name', 'required': True},
    )
    def put(self, _id, filename, local_name):
        import base.common.orm
        from src.models.blog import Post
        _session = self.orm_session

        p = _session.query(Post).filter(Post.id == _id).one_or_none()
        if not p:
            return self.error("Post not found")

        p.attached_files.append(PostFile(filename, local_name))

        _session.commit()

        return self.ok({"ok": True})

    @params(
        {'name': 'id', 'type': str, 'doc': 'id', 'required': True},
        {'name': 'file_names', 'type': list, 'doc': 'list of files to append', 'required': True},
    )
    def patch(self, _id, file_names):
        import base.common.orm
        from src.models.blog import Post
        _session = self.orm_session

        p = _session.query(Post).filter(Post.id == _id).one_or_none()
        if not p:
            return self.error("Post not found")

        for file in file_names:
            p.attached_files.append(PostFile(file['filename'], file['local_name']))

        _session.commit()

        return self.ok({"ok": True})



@api(
    URI='/posts/files-for-editor',
    SPECIFICATION_PATH='Blog'
)
@db()
class SaveFilesForEditor(Base):
    """Save file for the post from the ck editor"""

    @tornado.gen.coroutine
    def post(self):

        import src.config.blog_config as bc
        import uuid

        attach_files = self.request.files.get('upload')
        attached_file = attach_files[0]
        attached_file_name = attached_file['filename']
        attached_file_ext = attached_file_name.split('.')[-1]

        fn = uuid.uuid4()
        file_name = '{}/{}.{}'.format(bc.blog_files_directory, fn, attached_file_ext)
        with open(file_name, 'wb') as f:
            f.write(attached_file['body'])


        # todo: save changes to the database

        yield self.ok({
            'url': '{}/{}.{}'.format(bc.frontend_blog_files_directory, fn, attached_file_ext),
            'uploaded': 1,
            'fileName': attached_file_name
        })


@authenticated()
@api(
    URI='/posts/files',
    SPECIFICATION_PATH='Blog'
)
class SaveFiles(Base):
    """Save file for the post"""

    @tornado.gen.coroutine
    def post(self):

        id_post = self.get_argument('id_post', None)
        if id_post is None:
            return self.error('No post found')

        import src.config.blog_config as bc
        import uuid

        attached_files = []
        attach_files = self.request.files.get('files[]')
        if attach_files is None:
            return self.ok({'result': attached_files})

        for attached_file in attach_files:
            attached_file_name = attached_file['filename']
            attached_file_ext = attached_file_name.split('.')[-1]
            fn = uuid.uuid4()
            file_name = '{}/{}.{}'.format(bc.blog_files_directory, fn, attached_file_ext)
            with open(file_name, 'wb') as f:
                f.write(attached_file['body'])

            attached_files.append(
                {
                    'staticUrl': '{}/{}.{}'.format(bc.frontend_blog_files_directory, fn, attached_file_ext),
                    'uploaded': 1,
                    'fileName': attached_file_name,
                    'localFileName': '{}.{}'.format(fn, attached_file_ext),
                })

        files_to_set = [{'filename': f['fileName'], 'local_name': f['localFileName']} for f in attached_files]

        def set_files(files, _id_post):

            import base.common.orm
            from src.models.blog import Post
            _session = self.orm_session

            p = _session.query(Post).filter(Post.id == _id_post).one_or_none()
            if not p:
                return False

            for file in files:
                p.attached_files.append(PostFile(file['filename'], file['local_name']))

            try:
                _session.commit()
            except Exception as e:
                _session.rollback()
                return False

            return True

        executor = concurrent.futures.ThreadPoolExecutor(max_workers=20)

        response = yield executor.submit(set_files, files_to_set, id_post)
        if not response:
            return self.error('Error set files to post')

        return self.ok({'result': attached_files})


@authenticated()
@api(
    URI='/posts/cover',
    SPECIFICATION_PATH='Blog'
)
class SaveCover(Base):
    """Save Cover"""

    @tornado.gen.coroutine
    def post(self):

        id_post = self.get_argument('id_post', None)
        if id_post is None:
            return self.error('No post found')

        attach_files = self.request.files.get('files[]')
        if attach_files is None:
            return self.ok({'result': ''})

        import base.common.orm
        from src.models.blog import Post
        _session = self.orm_session

        p = _session.query(Post).filter(Post.id == id_post).one_or_none()
        if not p:
            return False

        attached_image = attach_files[0]
        cover_directory_for_frontend = '/static/uploaded_images'
        cover_directory_for_save = '{}{}'.format(os.getcwd(), cover_directory_for_frontend)

        attached_file_name = attached_image['filename']
        attached_file_ext = attached_file_name.split('.')[-1]
        fn = '{}_cover.{}'.format(p.slug, attached_file_ext)
        file_name = '{}/{}'.format(cover_directory_for_save, fn)

        with open(file_name, 'wb') as f:
            f.write(attached_image['body'])

        def set_file(file, _id_post):

            import base.common.orm
            from src.models.blog import Post
            _session = self.orm_session

            p = _session.query(Post).filter(Post.id == _id_post).one_or_none()
            if not p:
                return False

            p.cover_img = file

            try:
                _session.commit()
            except Exception as e:
                _session.rollback()
                return False

            return True

        file_name_for_frontend = '{}/{}'.format(cover_directory_for_frontend, fn)
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=20)

        response = yield executor.submit(set_file, file_name_for_frontend, id_post)
        if not response:
            return self.error('Error set files to post')

        return self.ok({'result': file_name_for_frontend})


@authenticated()
@api(
    URI='/posts/thumb',
    SPECIFICATION_PATH='Blog'
)
class SaveThumb(Base):
    """Save Thumb"""

    @tornado.gen.coroutine
    def post(self):

        id_post = self.get_argument('id_post', None)
        if id_post is None:
            return self.error('No post found')

        attach_files = self.request.files.get('files[]')
        if attach_files is None:
            return self.ok({'result': ''})

        import base.common.orm
        from src.models.blog import Post
        _session = self.orm_session

        p = _session.query(Post).filter(Post.id == id_post).one_or_none()
        if not p:
            return False

        attached_image = attach_files[0]
        cover_directory_for_frontend = '/static/images/blog_img'
        cover_directory_for_save = '{}{}'.format(os.getcwd(), cover_directory_for_frontend)

        attached_file_name = attached_image['filename']
        attached_file_ext = attached_file_name.split('.')[-1]
        fn = '{}_thumb.{}'.format(p.slug, attached_file_ext)
        file_name = '{}/{}'.format(cover_directory_for_save, fn)

        with open(file_name, 'wb') as f:
            f.write(attached_image['body'])

        def set_file(file, _id_post):

            import base.common.orm
            from src.models.blog import Post
            _session = self.orm_session

            p = _session.query(Post).filter(Post.id == _id_post).one_or_none()
            if not p:
                return False

            p.tumb_img = file

            try:
                _session.commit()
            except Exception as e:
                _session.rollback()
                return False

            return True

        file_name_for_frontend = '{}/{}'.format(cover_directory_for_frontend, fn)
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=20)

        response = yield executor.submit(set_file, file_name_for_frontend, id_post)
        if not response:
            return self.error('Error set files to post')

        return self.ok({'result': file_name_for_frontend})


ALL_IMAGES = expanduser("~")+'/Nikola/portal/static/uploaded-imgs/'


@authenticated()
@api(
    URI='/upload-blog-image/:id_post'
)
class HandleImageUploadForBlog(Base):
    @params(
        {'name': 'id_post', 'type': int, 'doc': 'profile image', 'required': True},
        {'name': 'image', 'type': str, 'doc': 'profile image', 'required': True},
        {'name': 'original_name', 'type': str, 'doc': 'profile image', 'required': False, 'default': "untitled"},
    )
    @tornado.gen.coroutine
    def post(self, id_post, base64_image_content, original_name):

        from src.models.blog import Post
        from src.models.site import SitePage

        # the_page = self.orm_session.query(SitePage).filter(SitePage.id == id_post).one_or_none()
        # if not the_page:
        #     yield self.error('page not found')
        #     return
        the_post = self.orm_session.query(Post).filter(Post.id==id_post).one_or_none()
        if not the_post:
            yield self.error('post not found')
            return

        _, img = base64_image_content.split(',')

        img = base64.b64decode(img)

        filesize = len(img)

        s = hashlib.sha256()
        s.update(img)

        f = NamedTemporaryFile(delete=False)
        f.write(img)
        f.close()

        img = PILImage.open(f.name)

        # target_fname = f'{s.hexdigest()}.{img.format}'.lower()
        target_fname = '{}.{}'.lower().format(s.hexdigest(), img.format)

        shutil.copyfile(f.name, ALL_IMAGES + target_fname)
        os.unlink(f.name)

        updated = False

        photo = {'fname': target_fname,
                 'width': img.size[0],
                 'height': img.size[1]}

        db_img = self.orm_session.query(Image).filter(
            Image.user == self.auth_user.user,
            Image.unique_filename == photo['fname']
        ).one_or_none()

        if not db_img:

            db_img = Image(self.auth_user.user, photo['fname'], filesize, img.size[0], img.size[1],
                                    original_name)
            self.orm_session.add(db_img)
            updated = True

        if not the_post.cover_img or the_post.cover_img != target_fname:
            the_post.cover_img = target_fname
            updated = True

        # if not the_page.icon or the_page.icon != target_fname:
        #     the_page.icon = target_fname
        #     updated = True

        if updated:
            try:
                self.orm_session.commit()
                log.info('image is updated')
            except Exception as e:
                yield self.error('excetpion '.format('e'))
                log.info('error updating image')
                return

        log.info('photo uploaded successfully.')
        yield self.ok(photo)


@authenticated()
@api(
    URI='/upload-blog-temp-image/:id_page'
)
class HandleImageUploadForTemplate(Base):
    @params(
        {'name': 'id_page', 'type': int, 'doc': 'profile image', 'required': True},
        {'name': 'image', 'type': str, 'doc': 'profile image', 'required': True},
        {'name': 'original_name', 'type': str, 'doc': 'profile image', 'required': False, 'default': "untitled"},
    )
    @tornado.gen.coroutine
    def post(self, id_page, base64_image_content, original_name):

        from src.models.blog import Post
        from src.models.site import SitePage

        # the_page = self.orm_session.query(SitePage).filter(SitePage.id == id_post).one_or_none()
        # if not the_page:
        #     yield self.error('page not found')
        #     return
        the_page = self.orm_session.query(SitePage).filter(SitePage.id==id_page).one_or_none()
        if not the_page:
            yield self.error('page not found')
            return

        _, img = base64_image_content.split(',')

        img = base64.b64decode(img)

        filesize = len(img)

        s = hashlib.sha256()
        s.update(img)

        f = NamedTemporaryFile(delete=False)
        f.write(img)
        f.close()

        img = PILImage.open(f.name)

        # target_fname = f'{s.hexdigest()}.{img.format}'.lower()
        target_fname = '{}.{}'.lower().format(s.hexdigest(), img.format)

        shutil.copyfile(f.name, ALL_IMAGES + target_fname)
        os.unlink(f.name)

        updated = False

        photo = {'fname': target_fname,
                 'width': img.size[0],
                 'height': img.size[1]}

        db_img = self.orm_session.query(Image).filter(
            Image.user == self.auth_user.user,
            Image.unique_filename == photo['fname']
        ).one_or_none()

        if not db_img:

            db_img = Image(self.auth_user.user, photo['fname'], filesize, img.size[0], img.size[1],
                                    original_name)
            self.orm_session.add(db_img)
            updated = True

        if not the_page.icon or the_page.icon != target_fname:
            the_page.icon = target_fname
            updated = True

        # if not the_page.icon or the_page.icon != target_fname:
        #     the_page.icon = target_fname
        #     updated = True

        if updated:
            try:
                self.orm_session.commit()
                log.info('image is updated')
            except Exception as e:
                yield self.error('excetpion '.format('e'))
                log.info('error updating image')
                return

        log.info('photo uploaded successfully.')
        yield self.ok(photo)


@api(URI='/contact')
class ContactUsHandler(Base):
    @params(
        {'name': 'first_name', 'type': str},
        {'name': 'last_name', 'type': str},
        {'name': 'email', 'type': str},
        {'name': 'tel', 'type': str},
        {'name': 'address', 'type': str},
        {'name': 'region', 'type': str},
        {'name': 'message', 'type': str},
    )
    def post(self, first_name, last_name, email, tel, address, region, message):
        # verovatno ce trebati neki csv fajl da se kreira
        return self.ok()


