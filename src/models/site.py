# coding= utf-8

import datetime

import base.common.orm
import timeago
from sqlalchemy import Column, String, Integer, Text, ForeignKey, CHAR, DateTime, Boolean
from sqlalchemy.orm import relationship
from src.models.user import User


class Page(base.common.orm.sql_base):
    """Site page data"""

    __tablename__ = 'site_page'

    id = Column(Integer, autoincrement=True, primary_key=True)
    url = Column(String(64), unique=True, nullable=False)
    html_meta = Column(Text(), nullable=False)

    def __init__(self, url, html_meta):
        self.url = url
        self.html_meta = html_meta


class Card(base.common.orm.sql_base):
    __tablename__ = 'cards'

    id = Column(Integer, autoincrement=True, primary_key=True)
    title = Column(String(255), nullable=False, index=True)
    url = Column(String(64), unique=True, nullable=False)
    image = Column(String(255), nullable=True, default=None)
    body = Column(Text)

    def __init__(self, title, url, image, body):
        self.title = title
        self.url = url
        self.image = image
        self.body = body


class SiteTemplate(base.common.orm.sql_base):
    __tablename__ = 'templates'

    id = Column(String(64), primary_key=True)
    name = Column(String(255), nullable=False, unique=True)

    def __init__(self, id, name):
        self.id = id
        self.name = name


class SiteTemplateSection(base.common.orm.sql_base):
    __tablename__ = 'template_sections'

    id = Column(String(64), primary_key=True)
    id_template = Column(ForeignKey(SiteTemplate.id), primary_key=True)

    template = relationship(SiteTemplate, uselist=False)

    name = Column(String(255), nullable=False)

    def __init__(self, template, id, name):
        self.template = template
        self.id = id
        self.name = name


class SitePage(base.common.orm.sql_base):
    __tablename__ = 'site_pages'

    id = Column(Integer, autoincrement=True, primary_key=True)
    title = Column(String(255), nullable=False, index=True)
    body = Column(Text)
    slug = Column(String(255), nullable=False, unique=True)
    language = Column(CHAR(2), nullable=False, index=True)
    created = Column(DateTime, nullable=False, default=datetime.datetime.now)
    icon = Column(String(255), nullable=True, default=None)
    group_pages = Column(Integer, nullable=False)
    status = Column(Boolean)
    # author = Column(String(255), nullable=False, index=True)
    id_user = Column(CHAR(10), ForeignKey(User.id), index=True)
    user = relationship("User", foreign_keys=[id_user])

    id_template = Column(ForeignKey(SiteTemplate.id), index=True, nullable=False)
    template = relationship(SiteTemplate, uselist=False)

    def date_created(self):
        return self.created.strftime("%d.%m.%Y")

    def ago(self):
        now = datetime.datetime.now() + datetime.timedelta(seconds=60 * 3.4)
        return timeago.format(self.created, now)

    def page_photo(self):
        if self.icon is not None:
            return '/static/uploaded-imgs/' + self.icon
        return '/static/img/pb.png'

    def body_text(self):
        if self.body is not None:
            return self.body
        return 'Odit est odis etur seces illis sim dolupta nobit unditate corepernam rem suntem quaepta simolup tatium nulluptatium dit ped magnatur re moluptat. Tus que occus, et omniam fugitatatur sunt aut ex excest, vendam quatiat. Itasi officie ntemodio. Udandel luptae il incimpo rehentiume milique ipsae a cum et pori nullis aut ipsumque simaiosandes volecea quassinum sitatiandis est dolupta tionsequo essunt. Odit est odis etur seces illis sim dolupta nobit unditate corepernam rem suntem quaepta simolup tatium nulluptatium dit ped magnatur re moluptat. Tus que occus, et omniam fugitatatur sunt aut ex excest, vendam quatiat. Itasi officie ntemodio.'

    # page_translations = relationship('SitePageTranslations', uselist=True, back_populates='page')

    def __init__(self, template, title, slug, language, group_pages, status):
        self.title = title
        self.slug = slug
        self.template = template
        self.language = language
        self.group_pages = group_pages
        self.status = status


# class SitePageTranslations(base.common.orm.sql_base):
#     __tablename__ = 'site_pages_translations'
#
#     id = Column(Integer, autoincrement=True, primary_key=True)
#     language = Column(String(2), index=True)
#     data = Column(JSONB, nullable=True, default=None)
#
#     id_page = Column(ForeignKey(SitePage.id), index=True)
#     page = relationship(SitePage, foreign_keys=[id_page], uselist=False, back_populates='page_translations')
#
#     def __init__(self, language, data, page):
#         self.language = language
#         self.data = data
#         self.page = page


def main():
    pass


if __name__ == '__main__':
    main()
