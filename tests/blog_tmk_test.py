# coding=utf-8
from tests.blog_tests import TestBlogBase
from base.tests.helpers.testing import db_state


class TestTmkBlog(TestBlogBase):

    @db_state(save_to='fuck')
    def test_0(self):
        id_user, token_user = self.reg('nikola@gmail.com', '123', {'first_name': 'Nikola',
                                                                   'last_name': 'Jovic',
                                                                   'data': '{}',
                                                                   'role_flags': 1})
        r = self.api(token_user, 'PUT', '/api/site/template', body={'id': 'template1',
                                                                    'name': 'product-template1',
                                                                    'structure': {
                                                                        'sections': [
                                                                            {
                                                                                'id': 'title',
                                                                                'name': 'Title',
                                                                            },
                                                                            {
                                                                                'id': 'features',
                                                                                'name': 'Product features'
                                                                            },
                                                                            {
                                                                                'id': 'contact'
                                                                            },
                                                                            {
                                                                                'id': 'more_features'
                                                                            },
                                                                            {
                                                                                'id': 'other_services'
                                                                            }
                                                                        ]
                                                                    }

                                                                    })
