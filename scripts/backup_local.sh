#!/bin/sh

# local
mkdir -p /Users/nikolajovic/work/web_blog/backups
fname='/Users/nikolajovic/work/web_blog/backups/backup-'`date +%Y-%m-%d-%H%M%S`'.sql'
pg_dump -U web_blog web_blog > $fname

gzip $fname