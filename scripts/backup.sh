#!/bin/sh

# server
mkdir -p /home/website/work/backups
fname='/home/website/work/backups/backup-'`date +%Y-%m-%d-%H%M%S`'.sql'
pg_dump -U website website > $fname

gzip $fname

