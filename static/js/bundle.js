console.log('car')
let emailRegex = new RegExp(/^[^\s@]+@[^\s@]+\.[^\s@]+$/);
let passRegex = new RegExp(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/)


function login(e) {
    e.preventDefault();
    const username = document.getElementById('username');
    const password = document.getElementById('password');
    let responseText = document.getElementById('response_text');

    // if (!emailRegex.test(username.value)) {
    //     responseText.textContent = 'wrong email'
    //     responseText.style.visibility = 'visible'
    //     username.value = ''
    //     password.value = ''
    //     return
    // }
    // if (!passRegex.test(password.value)) {
    //     responseText.textContent = 'wrong password'
    //     responseText.style.visibility = 'visible'
    //     username.value = ''
    //     password.value = ''
    //     return
    // }
    //
    if (!passRegex.test(password.value) && !emailRegex.test(username.value)) {
        responseText.textContent = 'email and password not valid';
        responseText.style.visibility = 'visible';
        username.value = ''
        password.value = ''
    }


    const _data = {
        username: username.value,
        password: password.value
    };


    $.ajax({
        url: `/user/login`,
        method: 'POST',
        data: JSON.stringify(_data),
        dataType: 'json'
    }).done(function (res) {
        localStorage.setItem('token', res.token);
        console.log('logovan');
        window.location.href = '/'
    }).fail(function (err) {
        console.log('Error: ', err);
        username.value = ''
        password.value = ''
    })
}


function register(e) {
    e.preventDefault();
    const username = document.getElementById('reg-username');
    const password = document.getElementById('reg-password');
    const firstName = document.getElementById('first-name');
    const lastName = document.getElementById('last-name');

    let responseText = document.getElementById('response_text');
    if (!passRegex.test(password.value) && !emailRegex.test(username.value)) {
        responseText.textContent = 'email and password not valid'
        responseText.style.visibility = 'visible'
        firstName.value = ''
        lastName.value = ''
        username.value = ''
        password.value = ''
    }

    const _data = {
        username: username.value,
        password: password.value,
        data: {
            'first_name': firstName.value,
            'last_name': lastName.value,
        }
    };

    console.log(_data)

    $.ajax({
        url: `/user/register`,
        method: 'POST',
        data: JSON.stringify(_data),
        dataType: 'json'
    }).done(function (res) {
        localStorage.setItem('token', res.token);
        console.log('logovan');
        responseText.classList.remove('error');
        responseText.textContent = '';
        // window.location.href = '/'
    }).fail(function (err) {
        console.log('Error: ', err);
        firstName.value = ''
        lastName.value = ''
        username.value = ''
        password.value = ''
    })
}


const loginBtn = document.getElementById('login');

if (loginBtn) {
    loginBtn.addEventListener('click', login)
}

const registerBtn = document.getElementById('register');

if (registerBtn) {
    registerBtn.addEventListener('click', register)
}


// function signin(ev) {
//     console.log('LOGIN', ev);
//     ev.preventDefault();
//
//     let _lang = window.location.pathname.split('/')[1];
//     if (_lang.length !== 2) {
//         _lang = '';
//     }
//
//     const _email = $('#email').val();
//     const _password = $('#password').val();
//
//     let regex = new RegExp(/^[^\s@]+@[^\s@]+\.[^\s@]+$/);
//     let responseText;
//     setTimeout(() => {
//         window.addEventListener('click', function () {
//             responseText.classList.remove('error');
//             responseText.innerHTML = '';
//         });
//         $('#log-in').click(function (e) {
//             e.stopPropagation();
//         });
//     }, 100);
//
//     console.log('WITH', _email, _password);
//     const _data = JSON.stringify({username: _email, password: _password});
//     if (!_email || !_password) {
//         responseText = document.getElementById('response__text');
//         responseText.innerHTML = 'unesi sva polja';
//         responseText.classList.add('error');
//     }
//     if (!_email) {
//         $('#email').focus();
//     } else if (!regex.test(_email)) {
//         responseText = document.getElementById('response__text');
//         responseText.innerHTML = 'pogresan mail';
//         responseText.classList.add('error');
//     } else if (!_password) {
//         $('#password').focus();
//     } else {
//         // spinnerLoader();
//         $.ajax({
//             url: '/api/user/login',
//             method: 'POST',
//             data: _data,
//             dataType: 'json'
//         }).done(function (res) {
//             console.log('LOGIN SUCCESS', res);
//             // alert('User ' + _email + ' logged in');
//             // localStorage.setItem('token', res['token']);
//             // debugger;
//             window.location.href = `/${_lang}`;
//             if (window.location.href === `/${_lang}`) {
//                 // document.querySelector('div.loader').classList.remove('loader');
//                 // document.querySelector('div.loader-opacity').classList.remove('loader-opacity');
//             }
//             return false;
//         }).fail(function (err) {
//             console.log('ERROR', err);
//             // document.querySelector('div.loader').classList.remove('loader');
//             // document.querySelector('div.loader-opacity').classList.remove('loader-opacity');
//             if (err.status === 401) {
//                 responseText = document.getElementById('response__text');
//                 responseText.innerHTML = 'asdfasdf';
//                 responseText.classList.add('error');
//                 // If we get error 401 that means that the user has slaff account but does not have acc on sacret life so
//                 // he needs to enter 6 digit code in order to login
//                 let loginCode = err.responseJSON.code;
//                 document.querySelector('.two_fa__wrapper').classList.add('opened');
//                 $('.two_fa__authentificator').fadeIn({
//                     start: function () {
//                         $(this).css({
//                             display: "flex"
//                         })
//                     }
//                 });
//
//                 // Function that collects copied text
//                 async function paste() {
//                     const text = await navigator.clipboard.readText();
//                         console.log(typeof(text));
//                         console.log(text);
//                         if(text == Number(text) && text.length === 6) {
//                             console.log('moze da se pretvori u brojeve');
//                             let arrList = text.split('');
//                             console.log(arrList);
//                             let inputFields = document.querySelectorAll('.auto-tab');
//                                 arrList.forEach((arr) => {
//                                     for(let i = 0; i < inputFields.length; i++) {
//                                         inputFields[i].value = arr;
//                                     }
//                                 });
//
//                         } else {
//                             console.log('ne moze')
//                         }
//                 }
//
//
//                 let authCode = [];
//                 let allInputFields = document.querySelectorAll('.two_fa__body--bottom input');
//
//                 $(".two_fa__body--bottom input:first").focus();
//                 $(".two_fa__body--bottom input").keydown(function (event) {
//                     if (event.originalEvent.key !== '' && event.keyCode !== 8) {
//                         setTimeout(() => {
//                             authCode.push(event.target.value);
//                             $(this).next('.two_fa__body--bottom input').focus();
//                             if (authCode.length === 6) {
//                                 $(this).blur();
//                                 // spinnerLoader();
//                                 authCode = authCode.join("");
//                                 $.ajax({
//                                     url: `/api/user/login?username=${_email}&two_factor_id=${loginCode}&two_factor_code=${authCode}&password=${_password}`,
//                                     method: 'POST',
//                                     data: _data
//                                 }).done(function (res) {
//                                     console.log(res);
//                                     window.location.href = `/${_lang}`;
//                                 }).fail(function (err) {
//                                     authCode = [];
//                                     // document.querySelector('div.loader').classList.remove('loader');
//                                     // document.querySelector('div.loader-opacity').classList.remove('loader-opacity');
//                                     toasterFront('los kod', 'error', 'click-confirm');
//                                     for (let i = 0; i < allInputFields.length; i++) {
//                                         allInputFields[i].value = '';
//                                     }
//                                 });
//                             }
//                         }, 20);
//                     } else if (event.keyCode === 8) {
//                         setTimeout(() => {
//                             $(this).prev('.two_fa__body--bottom input').focus();
//                             authCode.pop();
//                             console.log(authCode);
//                         }, 20);
//                     } else {
//                         return false;
//                     }
//                 });
//             } else {
//                 responseText = document.getElementById('response__text');
//                 responseText.innerHTML = 'los kod';
//                 responseText.classList.add('error');
//             }
//             return false;
//         });
//     }
// }
//
//
// function signUp(ev) {
//     console.log('SIGNUP', ev);
//     ev.preventDefault();
//
//     let _lang = window.location.pathname.split('/')[1];
//     if (_lang.length !== 2) {
//         _lang = '';
//     }
//
//     const _username = $('#email2').val();
//     const _password = $('#pass').val();
//     let _first_name = $('#first_name').val();
//     let _last_name = $('#last_name').val();
//     const _email = $('#email2').val();
//     let responseText = document.getElementById('response__text');
//     let emailRegex = new RegExp(/^[^\s@]+@[^\s@]+\.[^\s@]+$/);
//     let passRegex = new RegExp(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/)
//     setTimeout(() => {
//         window.addEventListener('click', function () {
//             responseText.classList.remove('error');
//             responseText.innerHTML = '';
//         });
//         $('#log-in').click(function (e) {
//             e.stopPropagation();
//         });
//     }, 100);
//     if (!_username || !_password || !_first_name || !_last_name) {
//         // if (!_username) {
//         //     $('#email2').focus();
//         // } else {
//         //     $('#pass').focus();
//         // }
//         responseText.classList.add('error');
//         return;
//     }
//     if (!emailRegex.test(_email)) {
//         responseText.classList.add('error');
//         return
//     }
//     if (!passRegex.test(_password)) {
//         responseText.classList.add('error');
//         return
//     }
//
//
//     console.log('WITH', _username, _password);
//     let _user_data = {
//         'first_name': $('#first_name').val(),
//         'last_name': $('#last_name').val()
//     };
//
//     const _hash = $("#registration-hash").attr('data-hash');
//     if (_hash !== undefined) {
//         _user_data['hash'] = _hash
//     }
//
//     const _data = JSON.stringify({username: _username, password: _password, data: JSON.stringify(_user_data)});
//
//     console.log('data', _data);
//     spinnerLoader();
//     $.ajax({
//         url: '/api/user/register',
//         method: 'POST',
//         data: _data,
//         dataType: 'json'
//     }).done(function (res) {
//         console.log('REGISTROVANO', res);
//         window.location.href = `/${_lang}`;
//         if (window.location.href === `/${_lang}`) {
//             document.querySelector('div.loader').classList.remove('loader');
//             document.querySelector('div.loader-opacity').classList.remove('loader-opacity');
//         }
//     }).fail(function (err) {
//         console.log('ERROR', err);
//
//     })
// }
//
// function attachSignin(element) {
//     if (element) {
//         console.log(element.id);
//         gAuth2.attachClickHandler(element, {},
//             onSignIn,
//             function (error) {
//                 console.log('DEBUGGING - ATTACHED SIGNIN ERROR; ' + JSON.stringify(error, undefined, 2));
//             });
//     }
// }
//
// $('#log-in').click(signin);
// $('#signup').click(signUp);
//


function logout() {
    const token = localStorage.getItem('token');
    const url = '/user/logout';
    let fetchData = {
        method: 'POST',
        headers: new Headers({'Authorization': token})
    };
    fetch(url, fetchData)
        .then(res => {
            console.log(res);
            $("#admin-header").css('display', 'none');
            // closeEditMode();
            localStorage.removeItem('token');
            localStorage.removeItem('edit');
            window.location.href = '/'
        })
        .catch(err => {
            console.log(err)
        })
}

function autoLogout() {
    let t;
    window.onload = resetTimer;
    window.onmousemove = resetTimer;
    window.onmousedown = resetTimer;
    window.ontouchstart = resetTimer;
    window.onclick = resetTimer;
    window.onkeypress = resetTimer;
    window.addEventListener('scroll', resetTimer, true);

    function resetTimer() {
        clearTimeout(t);
        t = setTimeout(logout, 600000);  // 10 minutes
    }
}

autoLogout();





// SIGN-OUT METHODS
function signOut() {

  let _lang = window.location.pathname.split('/')[1];
  if (_lang.length !== 2) {
    _lang = '';
  }

  // FB.logout(function(response) {
  // user is now logged out
  // if (gapi.auth2) {
    // var gAuth2 = gapi.auth2.getAuthInstance();
    // if (gAuth2.isSignedIn.get()) {
    //
    //   gAuth2.signOut().then(function () {
    //     console.log('User signed out.');
    //     if (document.cookie.indexOf('token') !== -1) {
    //       signOutWithToken();
    //     } else {
    //       window.location.href = `/${_lang}`;
    //     }
    //     // return false;
    //   }).catch(function (err) {
    //     console.log('User not signed out:', err);
    //     // localStorage.removeItem('token');
    //     document.cookie = "token= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
    //     window.location.href = `/${_lang}`;
    //     // return false;
    //   });
    //
    //   signOutWithToken();

    // }
  if (document.cookie.indexOf('token') !== -1) {
      signOutWithToken();

  }
  if (document.cookie.indexOf('token') !== -1) {
    signOutWithToken();
  }
  // });
  // return false;
}

function signOutWithToken() {

  let _lang = window.location.pathname.split('/')[1];
  if (_lang.length !== 2) {
    _lang = '';
  }
  // spinnerLoader();
  $.ajax({
    url: '/user/logout',
    method: 'POST',
    // headers: {'Authorization': localStorage.token}
  }).done(function (res) {
    console.log('LOGOUT SUCCESS', res);
    user = res;
    // localStorage.removeItem('token');
    document.cookie = "token= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
    // if (window.location.href === `/${_lang}`) {
      // document.querySelector('div.loader').classList.remove('loader');
      // document.querySelector('div.loader-opacity').classList.remove('loader-opacity');
    // }
    window.location.href = `/${_lang}`;
    // return false;
  }).fail(function (err) {
    console.log('ERROR', err);
    document.cookie = "token= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
    // if (window.location.href === `/${_lang}`) {
      // document.querySelector('div.loader').classList.remove('loader');
      // document.querySelector('div.loader-opacity').classList.remove('loader-opacity');
    // }
    window.location.href = `/${_lang}`;
    // return false;
  })
}

function resetSignUpUser() {
  $('#first_name').val('');
  $('#last_name').val('');
  $('#email2').val('');
  $('#pass').val('');
  $('#retype_password').val('');
  $('#affiliate_code').val('');
  return false;
}

let user = {};

function resetCurrentUser() {
  user = {};
}

// END OF SIGN-OUT METHODS
if (document.getElementById('add-post')) {


    document.getElementById('add-post').addEventListener('click', function (e) {
        e.preventDefault();
        let title = document.getElementById('post-title').value;
        let body = 'Odit est odis etur seces illis sim dolupta nobit unditate corepernam rem suntem quaepta simolup tatium nulluptatium dit ped magnatur re moluptat. Tus que occus, et omniam fugitatatur sunt aut ex excest, vendam quatiat. Itasi officie ntemodio. Udandel luptae il incimpo rehentiume milique ipsae a cum et pori nullis aut ipsumque simaiosandes volecea quassinum sitatiandis est dolupta tionsequo essunt. Odit est odis etur seces illis sim dolupta nobit unditate corepernam rem suntem quaepta simolup tatium nulluptatium dit ped magnatur re moluptat. Tus que occus, et omniam fugitatatur sunt aut ex excest, vendam quatiat. Itasi officie ntemodio.';
        let template_lang = document.getElementById('select-language').value;
        // let idTemplate = document.querySelector('input[name="radio-thumb"]:checked') ?
        //     document.querySelector('input[name="radio-thumb"]:checked').value : 'template1';

        const _data = {
            id_template: 'template1',
            title: title,
            body: body,
            slug: string_to_slug(title),
            language: template_lang
        };

        $.ajax({
            url: `/api/site/template/template1/pages`,
            method: 'PUT',
            data: _data,
            dataType: 'json'
        }).done(function (res) {
            console.log(res);
            // window.location.href = `/${template_lang}/products/${slug}`
        }).fail(function (err) {
            console.log('Error: ', err);
            return err
        })
    })
}

function string_to_slug(str) {
    str = str.replace(/^\s+|\s+$/g, "");
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    let from = "åàáãäâèéëêìíïîòóöôùúüûñçćčđšž·/_,:;";
    let to = "aaaaaaeeeeiiiioooouuuuncccdsz------";

    for (let i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
    }

    str = str
        .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
        .replace(/\s+/g, "-") // collapse whitespace and replace by -
        .replace(/-+/g, "-") // collapse dashes
        .replace(/^-+/, "") // trim - from start of text
        .replace(/-+$/, ""); // trim - from end of text

    return str;
}


(function () {

    var doc = document.documentElement;
    var w = window;

    var prevScroll = w.scrollY || doc.scrollTop;
    var curScroll;
    var direction = 0;
    var prevDirection = 0;

    var header = document.getElementById('site-header');

    var checkScroll = function () {

        /*
        ** Find the direction of scroll
        ** 0 - initial, 1 - up, 2 - down
        */

        curScroll = w.scrollY || doc.scrollTop;
        if (curScroll > prevScroll) {
            //scrolled up
            direction = 2;
        } else if (curScroll < prevScroll) {
            //scrolled down
            direction = 1;
        }

        if (direction !== prevDirection) {
            toggleHeader(direction, curScroll);
        }

        prevScroll = curScroll;
    };

    var toggleHeader = function (direction, curScroll) {
        if (direction === 2 && curScroll > 52) {

            //replace 52 with the height of your header in px

            header.classList.add('hide');
            prevDirection = direction;
        } else if (direction === 1) {
            header.classList.remove('hide');
            prevDirection = direction;
        }
    };

    window.addEventListener('scroll', checkScroll);

})();
const saveAll = document.querySelector('#save-all');

if (saveAll) {
    document.querySelectorAll('.post-wrapper').forEach(art => {
        saveAll.addEventListener('click', el => {
            let postID = art.getAttribute('id');
            let element = document.getElementById(postID);
            let title = element.getElementsByTagName('h4')[0].textContent;
            let body = element.getElementsByTagName('p')[0].innerHTML;

            const _data = {
                id: postID,
                title: title,
                body: body,
                language: 'sr',
            };

            console.log(_data);

            const url = `/api/wiki/posts/${postID}`;
            let fetchData = {
                method: 'PATCH',
                body: JSON.stringify(_data),
                headers: new Headers({
                    'Authorization': localStorage.getItem('token'),
                    'Content-Type': 'application/json'
                })
            };
            fetch(url, fetchData)
                .then(res => {
                    console.log(res);
                })
                .catch(err => {
                    console.log(err)
                });
        });
    })

    document.getElementById('save-all').addEventListener('click', function () {
        let pageId = document.querySelector('.news-article').getAttribute('data-page-id')

        const _data_page = {
            id: pageId,
            title: document.querySelector('.page-title').textContent,
            body: document.querySelector('.page-body').innerHTML,
            lang: 'sr',
        };

        const url_page = `/api/site/template/${pageId}/pages`;
        let fetchDataPage = {
            method: 'PATCH',
            body: JSON.stringify(_data_page),
            headers: new Headers({
                'Authorization': localStorage.getItem('token'),
                'Content-Type': 'application/json'
            })
        };
        fetch(url_page, fetchDataPage)
            .then(res => {
                console.log(res)
            })
            .catch(err => {
                console.log(err)
            });
    })
}





let editable = false;

if (document.getElementById('open-edit')) {
    document.getElementById('open-edit').addEventListener('click', enterEditMode);

}
if (document.getElementById('close-edit')) {
    document.getElementById('close-edit').addEventListener('click', exitEditMode);
}

function toggleBlue() {
    $('.page-wrapper').click(function (e) {
        var currentPost = $(this).find('h4');
        $('h4').not(currentPost).removeClass('pen');
        $('h4').siblings('p').not(currentPost).css('background-color', 'transparent');
        $('h4').not(currentPost).css('background-color', 'transparent');

        if (editable) {
            currentPost.addClass('pen');
            currentPost.css('background-color', 'rgba(136, 212, 255, 0.2)');
            currentPost.siblings('p').css('background-color', 'rgba(136, 212, 255, 0.2)');
        }
        e.stopPropagation();
    });
}

function enterEditMode() {
    toggleBlue();
    editable = true;
    localStorage.setItem('edit', 'true');
    $('#open-edit').css('display', 'none');
    $('#close-edit').css('display', 'flex');
    $('#save-all').css('display', 'flex');
    $('.editable-content').attr("contenteditable", true).css('outline', '1.5px dashed #3EB7FC').css('outline-offset', '3px');
    $('#toolbarLocation').css('display', 'block');
    $('.image').css('cursor', 'pointer');
    var elements = CKEDITOR.document.find('.editable-content'),
        i = 0,
        element;

    for (let name in CKEDITOR.instances) {
        delete CKEDITOR.instances[name];
    }

    // CKEDITOR.disableAutoInline = true;
    while ((element = elements.getItem(i++))) {
        CKEDITOR.inline(element, {
            sharedSpaces: {
                top: 'toolbarLocation'
            }
        });
    }

    // document.querySelectorAll('.cover-image').forEach(img => {
    //     img.setAttribute('src', '/static/img/pb.png')
    // })

}

function exitEditMode() {
    editable = false;
    localStorage.setItem('edit', 'false');

    $('#open-edit').css('display', 'flex');
    $('#close-edit').css('display', 'none');
    $('#save-all').css('display', 'none');
    $('.editable-content').attr("contenteditable", false).css('background', 'transparent').css('outline', 'none').css('outline-offset', '0');
    $('#toolbarLocation').css('display', 'none');
    $('.image').css('cursor', 'default');
    for (let name in CKEDITOR.instances) {
        delete CKEDITOR.instances[name];
    }
}


window.addEventListener("load", function () {
    if (localStorage.getItem('edit') === 'true' && localStorage.getItem('token')) {
        enterEditMode();
        toggleBlue()
    } else {
        exitEditMode();
    }
});

if (localStorage.getItem('token') && localStorage.getItem('edit') === 'true') {
    enterEditMode();
    toggleBlue()
} else {
    exitEditMode()
}
//
// document.querySelectorAll('.post-title').forEach(title => {
//     if (title.textContent.length === 0) {
//         title.style.display = 'none';
//     } else {
//         title.style.marginBottom = '-15px'
//     }
// });


// $(document).ready(function (e) {

//     $('.cards').slick({
//         lazyLoad: 'ondemand',
//         infinite: true,
//         arrows: false,
//         dots: true,
//         slidesToShow: 3,
//         slidesToScroll: 3,
//         responsive: [
//
//         ]
//     });
//
//
// // })



function openCropperModal() {
    document.getElementById("cropperModal").classList.add("opened");
}

function closeCropperModal() {
    document.getElementById("cropperModal").classList.remove("opened");
}

$(document).ready(function () {
    let article = document.querySelectorAll('.page-wrapper');
    for (let j = 0; j < article.length; j++) {

        let postID = article[j].getAttribute('id');
        let inputEl = document.createElement('input');
        //
        inputEl.setAttribute("type", "file");
        // inputEl.setAttribute("id", `input-${postID}`);
        article[j].appendChild(inputEl);
        inputEl.style.display = 'none';

        let imgs = article[j].querySelectorAll('.image');

        imgs.forEach(i => {
            i.addEventListener('click', function () {
                if (localStorage.getItem('edit') === 'true') {
                    inputEl.click()
                }
            });

            let canvas = $("#canvas");
            let context = canvas.get(0).getContext("2d");
            let result = $('#result');

            $(inputEl).on('change', function () {

                if (this.files && this.files[0]) {
                    if (this.files[0].type.match(/^image\//)) {
                        let reader = new FileReader();
                        reader.onload = function (evt) {
                            let img = new Image();
                            img.onload = function () {
                                context.canvas.height = img.height;
                                context.canvas.width = img.width;
                                context.drawImage(img, 0, 0);
                                let cropper = canvas.cropper({
                                    aspectRatio: $(inputEl).parent().find('.image').width() / $(inputEl).parent().find('.image').height(),
                                    viewMode: 1,
                                    fillColor: '#fff',
                                    crop(e) {
                                        document.getElementById('widthImage').textContent = e.detail.width.toFixed(0);
                                        document.getElementById('heightImage').textContent = e.detail.height.toFixed(0);
                                    }
                                });
                                setTimeout(() => {
                                    openCropperModal()
                                }, 200);
                                $('#btnRestore').click(function () {
                                    canvas.cropper('destroy');
                                    result.empty();
                                    closeCropperModal()
                                });
                                $('#btnCloseCropper').click(function () {
                                    canvas.cropper('destroy');
                                    result.empty();
                                    closeCropperModal()
                                });
                            };
                            img.src = evt.target.result;
                        };
                        reader.readAsDataURL(this.files[0]);


                        const cropImageBtn = document.querySelector('#crop-image');
                        cropImageBtn.addEventListener('click', function (art) {
                            let croppedImageDataURL = canvas.cropper('getCroppedCanvas', {fillColor: '#FFFFFF'}).toDataURL("image/jpeg", 0.9);
                            result.append($('<img>').attr('src', croppedImageDataURL));
                            // console.log(croppedImageDataURL);
                            let original_name = inputEl.files[0].name;

                            const _data = {
                                id_post: postID,
                                image: croppedImageDataURL,
                                original_name: original_name,
                            };


                            canvas.cropper('destroy');
                            result.empty();
                            i.src = croppedImageDataURL;
                            // removeSpinnerLoader();
                            $(inputEl).val('');
                            closeCropperModal();


                            document.querySelector('#save-all').addEventListener('click', function () {
                                let url = '';
                                if (article[j].getAttribute('data-page') === 'page-image') {
                                    url = `/api/upload-blog-temp-image/${postID}`
                                } else {
                                    url = `/api/upload-blog-image/${postID}`
                                }
                                $.ajax({
                                    url: url,
                                    method: 'POST',
                                    data: _data,
                                    headers: {"Authorization": localStorage.getItem('token')}
                                }).done(function (res) {
                                    console.log(res);
                                }).fail(function (err) {
                                    console.log('Error: ', err);
                                })
                            })

                        })
                    } else {
                        console.log("Invalid file type! Please select an image file.");
                    }
                } else {
                    console.log('No file(s) selected.');
                }
            })
        })
    }
});
// copy link from url
function copyLink() {
    let link = document.getElementById('shareUrl');
    link.value = window.location.href.toString();
    link.select();
    document.execCommand("copy");
    toaster('Kopiran URL', 'success');
}


function PopupCenter(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    let dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screenX;
    let dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screenY;

    let width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    let height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    let systemZoom = width / window.screen.availWidth;
    let left = (width - w) / 2 / systemZoom + dualScreenLeft;
    let top = (height - h) / 2 / systemZoom + dualScreenTop;
    let newWindow = window.open(url, title, 'scrollbars=yes, width=' + w / systemZoom + ', height=' + h / systemZoom + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) newWindow.focus();
}

function openSocialWindow(url) {
    PopupCenter(url, 'Share via', '600', '600');
}

let currentPageUrl = encodeURIComponent(document.URL);
// let tweet = encodeURIComponent($("meta[property='og:title']").attr("content"));

$(".facebook").on("click", function (url) {
    url = "https://www.facebook.com/sharer.php?u=" + currentPageUrl;
    openSocialWindow(url);
});

$(".twitter").on("click", function (url) {
    url = "https://twitter.com/intent/tweet?url=" + currentPageUrl;
    openSocialWindow(url);
});


$(".linkedin").on("click", function () {
    url = "http://www.linkedin.com/shareArticle?mini=true&url=" + currentPageUrl;
    openSocialWindow(url);
});