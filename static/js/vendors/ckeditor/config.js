/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function (config) {


    // The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbarGroups = [

        // {name: 'clipboard', groups: ['clipboard', 'undo', 'bold']},
        // {name: 'editing', groups: ['find', 'selection']},
        {name: 'basicstyles', groups: ['basicstyles']},
        {name: 'paragraph', groups: []},
        // {name: 'styles'},
        {name: 'colors'},
        {name: 'links'},
        // {name: 'insert'}, da enablujes slike
        {name: 'forms'},
        {name: 'tools'},
        // {name: 'editing', groups: ['find', 'selection', 'spellchecker']},
        // {name: 'forms'},
        // {name: 'tools'},
        // {name: 'document', groups: ['mode', 'document', 'doctools']},
        // {name: 'others'},

        // {name: 'about'}
    ];

    // Remove some buttons provided by the standard plugins, which are
    // not needed in the Standard(s) toolbar.
    config.removeButtons = '';

    // Set the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre';

    // Simplify the dialog windows.
    config.removeDialogTabs = 'image:advanced;link:advanced';



    // plugins
    config.extraPlugins = 'justify,uploadimage,youtube,sharedspace,pastefromword';
    // 'fixed,uploadimage,panelbutton,colorbutton,justify,colordialog,sourcedialog,font';

    // custom config
    config.customConfig = '/static/js/vendors/ckeditor/ckexternal.config.js';
    // image upload url on our dcbase blog api (api/blog/files.py)
    config.filebrowserImageUploadUrl = '/api/posts/files-for-editor';
    // Attachment upload url on our dcbase api (api/blogfiles.py)
    config.filebrowserUploadUrl = '/api/posts/files-for-editor';

};
