"""added images

Revision ID: 571dce4b4319
Revises: c9baacecfd50
Create Date: 2020-03-29 19:21:59.965023

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '571dce4b4319'
down_revision = 'c9baacecfd50'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('images',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('id_user', sa.CHAR(length=10), nullable=False),
    sa.Column('unique_filename', sa.String(length=70), nullable=True),
    sa.Column('original_filename', sa.String(length=128), nullable=True),
    sa.Column('filetype', sa.String(length=8), nullable=True),
    sa.Column('filesize', sa.Integer(), nullable=False),
    sa.Column('width', sa.Integer(), nullable=False),
    sa.Column('height', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['id_user'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_images_id_user'), 'images', ['id_user'], unique=False)
    op.create_index(op.f('ix_images_unique_filename'), 'images', ['unique_filename'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_images_unique_filename'), table_name='images')
    op.drop_index(op.f('ix_images_id_user'), table_name='images')
    op.drop_table('images')
    # ### end Alembic commands ###
