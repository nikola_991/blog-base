"""empty message

Revision ID: b16afee8c757
Revises: 02bb7d542fc3
Create Date: 2020-06-08 15:55:58.849173

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b16afee8c757'
down_revision = '02bb7d542fc3'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
