"""initial

Revision ID: 370430cb8a98
Revises: 087697437354
Create Date: 2020-04-01 14:04:52.090844

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '370430cb8a98'
down_revision = '087697437354'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
